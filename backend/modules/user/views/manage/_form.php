<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput() ?>
    <?= $form->field($model, 'roles')->checkboxList($model->getRolesDropdown()) ?>

    <?= $form->field($model, 'email')->textInput() ?>
    <?= $form->field($model, 'phone')->textInput() ?>
    <?= $form->field($model, 'status')->textInput() ?>
    <?= $form->field($model, 'fullname')->textInput() ?>
    <?= $form->field($model, 'nickname')->textInput() ?>
    <?= $form->field($model, 'position')->dropDownList($model->getPositionDropdown()) ?>
    <?= $form->field($model, 'department')->dropDownList($model->getDepartmentDropdown()) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
