<?php

/* @var $this yii\web\View */

$this->title = 'MWI Admin';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Admin panel</h1>

        <p class="lead">Users and DB management</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>DB Settings</h2>

                <p>Connections, backup polisies restore data and other DB issues. MYSQL management and queries editor</p>

                <p><a class="btn btn-default" href="/admin/db">DB Manager &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Users</h2>

                <p>Users management, roles, polisies, RBAC and history. Sendlists and email settings as well</p>

                <p><a class="btn btn-default" href="/admin/user/manage">Users&raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Environment</h2>

                <p>Server properties, environment variables, system updates, code repo and PHP settings.</p>

                <p><a class="btn btn-default" href="/admin/server">Server Settings &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
