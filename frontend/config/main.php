<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'name' => 'Mofet Web Interface',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'jb' => [
            'class' => 'frontend\modules\jb\Module',
        ],
        'user' => [
            'class' => 'frontend\modules\user\Module',
        ],
        'net' => [
            'class' => 'frontend\modules\net\Module',
        ],
        'node' => [
            'class' => 'frontend\modules\node\Module',
        ],
        'ucn' => [
            'class' => 'frontend\modules\ucn\Module',
        ],
        'pm' => [
            'class' => 'frontend\modules\pm\Module',
        ],
        'plant' => [
            'class' => 'frontend\modules\plant\Module',
        ],
        'card' => [
            'class' => 'frontend\modules\card\Module',
        ],
        'tag' => [
            'class' => 'frontend\modules\tag\Module',
        ],
        'gridview' => [
            'class' => 'kartik\grid\Module'
        ],
        'reports' => [
            'class' => 'frontend\modules\reports\Module',
        ],

        'aspen' => [
            'class' => 'frontend\modules\aspen\Module',
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => '',
        ],
        'user' => [
            'identityClass' => 'frontend\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages', // if advanced application, set @frontend/messages
                    'sourceLanguage' => 'en',
                    'fileMap' => [
                        //'main' => 'main.php',
                    ],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/site/login' => '/user/default/login',
                '/profile/<nickname:\w+>' => 'user/profile/view',
                'net/<id:\d+>' => 'net/default/view',
                'net/<id:\d+>/<node_num:\d+>' => 'node/default/view',
                'net/<net_id:\d+>/ucn/<ucn_id:\d+>' => 'ucn/default/view',
                'net/<net_id:\d+>/pm' => 'pm/default/index',
                'net/<net_id:\d+>/ucn/show' => 'ucn/default/show',
                //  'ucn/show/<net_id:\d+>' => 'ucn/default/show',
                'pm/<pm_id:\d+-\d+-\d+>' => 'pm/default/view',
                'pm/<pm_id:\d+-\d+-\d+>/layout' => 'pm/default/layout',
                'plant/<plant_name:\d+>' => 'plant/default/view',
                'card/<pm_id:\d+-\d+-\d+>/<card_num:\d+>' => 'card/default/view',
                'tag/view/<tag_name:\w+>' => 'tag/manage/view',
                'tag/create/<plant:\d+>/<pm_num:\d+>/<card:\d+>/<slot:\d+>/<net_id:\d+>/<ucn_id:\d+>/<type:\d+>' => 'tag/manage/create',
                'tag/update/<tag_name:\w+>' => 'tag/manage/update',
                'tag/delete/<tag_name:\w+>' => 'tag/manage/delete',
                'jb/create' => 'jb/default/create',
                'jb/view' => 'jb/default/view',
                'jb/update' => 'jb/default/update',
                'jb/delete' => 'jb/default/delete',
                'jb/overview' => 'jb/default/overview',
                'reports' => 'reports/default/',
                'aspen' => 'aspen/default/index',
                'aspen/view/tag<tag:\d+>/map<map:\w+>' => 'aspen/default/view',
                'aspen/view<tag:\d+><map:\w+>' => 'aspen/default/view',
            ],
        ],
    ],
    'params' => $params,
];
