<?php
/**
 * @var $this use yii\web\View;
 * @var $net common\models\Net
 */
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="container">

    <h1>
          <?php echo Html::encode($net->type); ?>
        <?php echo Html::encode($net->name); ?>
    </h1>
    <div class="row">
        <div class="col-mid-12">
            <strong>Description : </strong>
            <?php echo Html::encode($net->description); ?>
        </div>

        <div class="col-mid-12">
            <strong>NCF Time Stamp : </strong>
            <?php echo Html::encode($net->time); ?>
        </div>

        <div class="col-mid-12">
                <h5><a href="<?php echo Url::to(['/ucn']); ?>">
                    <strong>All UCNs &nbsp </strong>
                </a>

                &nbsp
                <a href="<?php echo Url::to(['/net/' . $net->id . '/pm']); ?>">
                    <strong>&nbsp All Net
                        <?php echo Html::encode($net->name); ?> PMs </strong>
                </a></h5>

        </div>

        <hr>


        <div class="row">
            <div class="col-lg-12">

                <?php foreach ($nodes as $node) : ?>
                <?php if ($node->type_id == 1): ?>
                <?php $class = ' card-outline-danger card-success ';?>
                <?php else: $class = ' card-outline-success card-info';?>
                <?php endif;?>
                <div class="card card-hover<?php echo $class?> text-center" style="width: 18rem; text-align:center;display:inline-block;">
                    <div class="card-header">
                        <h4 class="card-title"><a href="<?php echo Url::to(['/net/' . $net->id . '/' . $node->node_num]); ?>">
                                <?php echo Html::encode($node->getTypeName($node->type_id) . ' ' . $node->node_num); ?>
                            </a></h4>
                    </div>
                    <div class="card-body">

                        <p class="card-text">
                            <?php echo Html::encode($node->getTypeDescription($node->type_id)); ?>
                        </p>

                        <p class="card-text">

                            <?php if (isset($node->ucn_id)) : ?>
                            <a href="<?php echo Url::to(['/net/' . $net->id . '/ucn/' . $node->ucn_id]); ?>">
                                <?php echo 'UCN ' . Html::encode($node->ucn_id); ?>
                            </a>
                            <?php else :?>

                            <?php endif;?>
                            <br>

                            <?php  $arr = explode('/', $node->getPlantName());?>
                            <a href="<?php echo Url::to(['/plant/' . end($arr)]); ?>">
                                <?php echo Html::encode(end($arr)); ?>
                            </a>







                        </p>

                    </div>



                </div>


                <?php endforeach; ?>

            </div>


        </div>



    </div>

</div>