<?php

namespace frontend\modules\net\controllers;

use yii\web\Controller;
use common\models\Net;
use common\models\Node;

/**
 * Default controller for the `net` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $nets = Net::find()->all();
        $nodes = Node::find()->all();
        return $this->render('index', [
            'nets' => $nets,
            'nodes' => $nodes,
        ]);
    }

    /**
     * Renders the  view for the single lcn
     * @return string
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'net' => Net::findNet($id),
            'nodes' => Node::findAll(['net_id' => $id]),
        ]);
    }
}
