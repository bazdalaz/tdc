<?php

namespace frontend\modules\user\models\forms;

use yii\base\Model;
use frontend\models\User;
use Yii;

/**
 * Signup form
 */
class UpdateForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $fullname;
    public $nickname;
    public $phone;
    public $department;
    public $position;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            //    ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            //['email', 'unique', 'targetClass' => 'frontend\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['phone', 'integer'],
            ['password', 'string', 'min' => 6],

            ['fullname', 'trim'],
            ['nickname', 'trim'],
            ['position', 'trim'],
            ['department', 'trim'],
        ];
    }

    /**
     * Update user.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function update()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = User::findOne(['email' => Yii::$app->user->identity->email]);

        $user->username = $this->username;
        $user->email = $user->email;
        $user->fullname = $this->fullname;
        $user->phone = $this->phone;
        $user->nickname = $this->nickname;
        $user->position = $this->position;
        $user->department = $this->department;
        if ($user->validatePassword($this->password)) {
            return $user->save() ? $user : null;
        } else {
            return null;
        }
        // $user->setPassword($this->password);
        // $user->generateAuthKey();
    }
}
