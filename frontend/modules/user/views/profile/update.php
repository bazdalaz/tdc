<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\UpdateForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Update Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-update">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please update follow user info:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-update']); ?>

                <?= $form->field($model, 'username')->textInput(['value' => $user->username, 'autofocus' => true]) ?>
                <?= $form->field($model, 'fullname')->textInput(['value' => $user->fullname]) ?>
                <?= $form->field($model, 'phone')->textInput(['value' => $user->phone]) ?>
                <?= $form->field($model, 'nickname')->textInput(['value' => $user->nickname]) ?>
                <?= $form->field($model, 'email')->textInput(['value' => $user->email, 'disabled' => true]) ?>
                <?= $form->field($model, 'position')->dropDownList($user->getPositionDropDown()) ?>
                <?= $form->field($model, 'department')->dropDownList($user->getDepartmentDropDown()) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
