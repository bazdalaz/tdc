<?php
/**
 * @var $this use yii\web\View;
 * @var $node common\models\Node;
 */
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Tags';
$this->params['breadcrumbs'][] = $this->title ;

?>

<div class="lcn-single-view">
    <h1>LCN Tags:
    </h1>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'label' => 'Tag',
                            'format' => 'raw',
                            'value' => function ($model) {
                                $url = '/tag/' . $model->tag_name;
                                return Html::a($model->tag_name, $url, ['title' => 'Tag']);
                            }
                        ],

                        [
                            'label' => 'Description',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return hebrevc($model->description);
                            }
                        ],
                        [
                            'attribute' => 'type_id',
                            'headerOptions' => ['style' => 'width:5%'],
                            'value' => function ($model) {
                                return $model->type->type_name;
                            },
                            'label' => 'Type',
                            'filter' => common\models\TagType::getTypeList(),
                        ],
                        [
                            'label' => 'LCN',
                            'attribute' => 'net_id',
                            'value' => function ($model) {
                                return $model->net_id;
                            },
                        ],
                        [
                            'label' => 'UCN',
                            'attribute' => 'ucn_id',
                            'value' => function ($model) {
                                return $model->ucn_id;
                            },
                        ],
                        [
                            'label' => 'Plant',
                            'format' => 'raw',
                            'value' => function ($model) {
                                $url = '/plant/' . $model->plant;
                                return Html::a($model->plant, $url, ['title' => 'Plant']);
                            }
                        ],
                        [
                            'label' => 'PM',
                            'format' => 'raw',
                            'value' => function ($model) {
                                $url = '/pm/' . $model->net_id . '-' . $model->ucn_id . '-' . $model->pm_num;
                                return Html::a($model->pm_num, $url, ['title' => 'PM']);
                            }
                        ],
                        [
                            'label' => 'PM Card',
                            'value' => 'card_id',
                        ],
                        [
                            'label' => 'PM Slot',
                            'value' => 'slot_id',
                        ],
                        [
                            'label' => 'DI_1',
                            'value' => 'card_di_1',
                        ],
                        [
                            'label' => 'DI_2',
                            'value' => 'slot_di_2',
                        ],
                        [
                            'label' => 'Card DO_1',
                            'value' => 'card_do_1',
                        ],
                        [
                            'label' => 'DO_2',
                            'value' => 'slot_do_2',
                        ],
                        [
                            'label' => 'CI_1',
                            'value' => 'ci_1',
                        ],
                        [
                            'label' => 'Card AO_1',
                            'value' => 'card_ao_1',
                        ],
                        [
                            'label' => 'Slot AO_1',
                            'value' => 'slot_ao_1',
                        ],
                    ],
                ]) ?>


            </div>
        </div>
    </div>
</div>