<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Tag */

$this->title = $model->tag_name;
$this->params['breadcrumbs'][] = ['label' => 'Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tag-view">

    <h1><?= Html::encode($this->title) ?>
    </h1>

    <p>
        <?= Html::a('Update', ['update', 'tag_name' => $model->tag_name], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'tag_name' => $model->tag_name], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tag_name',
            'description',
            'plant',
            [
                'attribute' => 'type_id',
                'value' => function ($model) {
                    return $model->type->type_name;
                },
                'label' => 'Type',
            ],
            [
                'attribute' => 'net_id',
                'value' => function ($model) {
                    return $model->net->name;
                },
                'label' => 'LCN',
            ],
            [
                'attribute' => 'ucn_id',
                'value' => function ($model) {
                    return $model->ucn_id;
                },
                'label' => 'UCN',
            ],
            'pm_num',
            'card_id',
            'slot_id',
            'card_di_1',
            'slot_di_1',
            'card_di_2',
            'slot_di_2',
            'card_do_1',
            'slot_do_1',
            'card_do_2',
            'slot_do_2',
            'ci_1',
            'card_ao_1',
            'slot_ao_1',
            'ai_hi',
            'ai_low',
            'updated_by',
            [
                'attribute' => 'status_id',
                'value' => function ($model) {
                    return $model->status->status;
                },
                'label' => 'Status',
            ],
            'updated_at',
            'created_at',
            'note',
        ],
    ]) ?>

</div>