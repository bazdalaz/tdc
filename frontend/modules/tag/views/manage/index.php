<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\grid\GridView;

//use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tags';
$this->params['breadcrumbs'][] = $this->title;
?>


<?php

$gridColumns = [
    [
        'class' => 'kartik\grid\ActionColumn',
        'hiddenFromExport' => true,

        'template' => ' {view} ', //{update} {delete}',
        'buttons' => [
            'view' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-expand"></span>', $url, [
                    'title' => Yii::t('app', 'view'),
                ]);
            },

            // 'update' => function ($url, $model) {
            //     return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
            //                 'title' => Yii::t('app', 'update'),
            //     ]);
            // },
            // 'delete' => function ($url, $model) {
            //     return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
            //                 'title' => Yii::t('app', 'delete'),
            //     ]);
            // }
        ],
        'urlCreator' => function ($action, $model, $key, $index) {
            if ($action === 'view') {
                $url = '/tag/view/' . $model->tag_name;
                return $url;
            }

            // if ($action === 'update') {
          //     $url ='/tag/update/'.$model->tag_name;
          //     return $url;
          // }
          // if ($action === 'delete') {
          //     $url ='/tag/delete/'.$model->tag_name;
          //     return $url;
          // }
        }
    ],

    [
        'attribute' => 'tag_name',
        'headerOptions' => ['style' => 'width:5%'],
    ],
    [
        'attribute' => 'description',
        'headerOptions' => ['style' => 'width:10%'],
    ],
    [
        'attribute' => 'plant',
        'headerOptions' => ['style' => 'width:5%'],
        'filter' => common\models\Plant::getPlantList(),
    ],
    [
        'attribute' => 'type_id',
        'headerOptions' => ['style' => 'width:5%'],
        'value' => function ($model) {
            return $model->type->type_name;
        },
        'label' => 'Type',
        'filter' => common\models\TagType::getTypeList(),
    ],
    [
        'attribute' => 'net_id',
        'headerOptions' => ['style' => 'width:4%'],
        'value' => function ($model) {
            return $model->net->name;
        },
        'filter' => common\models\Net::getNetList(),
        'label' => 'LCN ',
    ],
    [
        'attribute' => 'ucn_id',
        'value' => function ($model) {
            return $model->ucn_id;
        },
        'label' => 'Ucn',
        // 'filter' => common\models\Ucn::getUcnList(),
    ],
    [
        'attribute' => 'pm_num',
        'headerOptions' => ['style' => 'width:4%'],
        'value' => function ($model) {
            return $model->pm_num;
        },
        'label' => 'PM',
        'filter' => common\models\Pm::getPmList($searchModel->plant),
    ],

    [
        'attribute' => 'card_id',
        'headerOptions' => ['style' => 'width:5%'],
    ],
    [
        'attribute' => 'slot_id',
        'headerOptions' => ['style' => 'width:5%'],
    ],
    [
        'attribute' => 'card_di_1',
        'label' => 'CARD 1',
        'headerOptions' => ['style' => 'width:5%'],
    ],
    [
        'attribute' => 'slot_di_1',
        'label' => 'DI 1',
        //  'headerOptions' => ['style' => 'width:5%'],
    ],

    [
        'attribute' => 'card_di_2',
        'label' => 'CARD 2',
        'headerOptions' => ['style' => 'width:5%'],
    ],
    [
        'attribute' => 'slot_di_2',
        'label' => 'DI 2',
        //   'headerOptions' => ['style' => 'width:5%'],
    ],
    [
        'attribute' => 'card_do_1',
        'label' => 'CARD 1',
        'headerOptions' => ['style' => 'width:5%'],
    ],
    [
        'attribute' => 'slot_do_1',
        'label' => 'DO 1',
        //   'headerOptions' => ['style' => 'width:5%'],
    ],
    [
        'attribute' => 'card_do_2',
        'label' => 'CARD 2',
        'headerOptions' => ['style' => 'width:5%'],
    ],
    [
        'attribute' => 'slot_do_2',
        'label' => 'DO 2',
        // 'headerOptions' => ['style' => 'width:5%'],
    ],
    'ci_1',

    [
        'attribute' => 'card_ao_1',
        'label' => 'Card AO',
        'headerOptions' => ['style' => 'width:5%'],
    ],
    [
        'attribute' => 'slot_ao_1',
        'label' => 'AO 1',
        'headerOptions' => ['style' => 'width:5%'],
    ],
    [
        'attribute' => 'status_id',
        // 'headerOptions' => ['style' => 'width:5%'],
        'value' => function ($model) {
            return $model->status->status;
        },
        'label' => 'Status',
        'filter' => common\models\TagStatus::getTagStatusList(),
    ],
]?>


<?php Pjax::begin();?>

<?php echo GridView::widget([
    // 'id' => 'tag-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    // 'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
    'responsive' => true,
    'pjax' => true,
    'pjaxSettings' => [
        'neverTimeout' => true,
    ],
    // 'perfectScrollbar' => true,
    'beforeHeader' => [
        [
            'columns' => [
                ['content' => 'Tag', 'options' => ['colspan' => 3, 'class' => 'text-center secondary']],
                ['content' => 'Address', 'options' => ['colspan' => 7, 'class' => 'text-center info']],
                ['content' => 'Digital Input', 'options' => ['colspan' => 4, 'class' => 'text-center secondary']],
                ['content' => 'Digital Output', 'options' => ['colspan' => 4, 'class' => 'text-center info']],
                ['content' => 'Analog I/O', 'options' => ['colspan' => 3, 'class' => 'text-center secondary']],
                ['content' => '', 'options' => ['colspan' => 1, 'class' => 'text-center info']],
            ],
            // 'options' => ['class' => 'skip-export'] // remove this row from export
        ],
    ],
    'resizableColumns' => true,

    'toolbar' => [
        [
            'content' => Html::button('<i class="glyphicon glyphicon-plus"></i>', [
                'type' => 'button',
                'onclick' => "window.location=(['/tag/manage/create'])",
                'title' => Yii::t('kvgrid', 'Add New Tag - no affect the TDC DB'),
                'class' => 'btn btn-primary',
                //  'class'=>'btn btn-success'
            ]) . ' ' .
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['/tag/manage/'], [
                    'class' => 'btn btn-outline-primary',
                    'title' => Yii::t('kvgrid', 'Reset Grid')
                ])
        ],
        '{toggleData}',
        'toggleDataContainer' => ['class' => 'btn-group-sm'],
        '{export}',
        'exportContainer' => ['class' => 'btn-group-sm'],
    ],

    // set export properties
    'export' => [
        //'fontAwesome' => true,
        'hiddenColumns' => [0, 9], // SerialColumn & ActionColumn
        'disabledColumns' => [1, 2], // ID & Name
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-secondary'
        ]
    ],

    // parameters
    //'bordered' => true,
    'floatHeader' => false,
    'floatHeaderOptions' => [
        'scrollingTop' => '50',
        //   'id' => 'tags',
    ],
    'striped' => true,
    'condensed' => false,
    'responsive' => false,
    'hover' => true,
    'showPageSummary' => false,
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => true,
        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-tags"></i>&nbsp Tags</h3>',
    ],
    'persistResize' => false,
    'toggleDataOptions' => ['minCount' => 32],
    // 'exportConfig' => $exportConfig,
    'itemLabelSingle' => 'tag',
    'itemLabelPlural' => 'tags',

    'rowOptions' => function ($model) {
        if ($model->status_id == 2) {
            return ['class' => 'danger'];
        }
        if ($model->status_id == 3) {
            return ['class' => 'info'];
        }
        if ($model->status_id == 4) {
            return ['class' => 'success'];
        }
        if ($model->status_id > 4) {
            return ['class' => 'warning'];
        }
    },

    'columns' => $gridColumns,
    'itemLabelSingle' => 'tag',
    'itemLabelPlural' => 'tags'
]); ?>
<?php Pjax::end();
