<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Tag */

?>
<div class="tag-view">

    <h1><?= Html::encode($model->tag_name) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>




</div>
