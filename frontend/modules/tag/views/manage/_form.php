<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Tag */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tag-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tag_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'plant')->dropdownList(
    \common\models\Plant::getPlantList(),
    [
        'prompt' => 'Select Plant',
        // 'onchange' => '$.post',
    ]
); ?>

    <?= $form->field($model, 'type_id')->dropdownList(\common\models\TagType::getTypeList()); ?>

    <?= $form->field($model, 'net_id')->dropdownList(\common\models\Net::getNetList()); ?>

    <?= $form->field($model, 'ucn_id')->dropdownList(\common\models\Ucn::getUcnList($model->ucn_id)) ; ?>

    <?= $form->field($model, 'pm_num')->dropdownList(\common\models\Pm::getPmList($model->plant)); ?>

    <?= $form->field($model, 'card_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slot_id')->textInput() ?>

    <?= $form->field($model, 'card_di_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slot_di_1')->textInput() ?>

    <?= $form->field($model, 'card_di_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slot_di_2')->textInput() ?>

    <?= $form->field($model, 'card_do_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slot_do_1')->textInput() ?>

    <?= $form->field($model, 'card_do_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slot_do_2')->textInput() ?>

    <?= $form->field($model, 'ci_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'card_ao_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slot_ao_1')->textInput() ?>

    <?= $form->field($model, 'ai_hi')->textInput(); ?>

    <?= $form->field($model, 'ai_low')->textInput(); ?>

    <?= $form->field($model, 'updated_by')->textInput(); ?>
    <?= $form->field($model, 'note')->textarea(); ?>

    <?= $form->field($model, 'status_id')->dropdownList(\common\models\TagStatus::getTagStatusList()); ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
