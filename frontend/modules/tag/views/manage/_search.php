<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TagSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tag-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tag_name') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'plant') ?>

    <?= $form->field($model, 'type_id') ?>

    <?php // echo $form->field($model, 'net_id')?>

    <?php // echo $form->field($model, 'ucn_id')?>

    <?php // echo $form->field($model, 'pm_num')?>

    <?php // echo $form->field($model, 'card_id')?>

    <?php // echo $form->field($model, 'slot_id')?>

    <?php // echo $form->field($model, 'card_di_1')?>

    <?php // echo $form->field($model, 'slot_di_1')?>

    <?php // echo $form->field($model, 'card_di_2')?>

    <?php // echo $form->field($model, 'slot_di_2')?>

    <?php // echo $form->field($model, 'card_do_1')?>

    <?php // echo $form->field($model, 'slot_do_1')?>

    <?php // echo $form->field($model, 'card_do_2')?>

    <?php // echo $form->field($model, 'slot_do_2')?>

    <?php // echo $form->field($model, 'ci_1')?>

    <?php // echo $form->field($model, 'card_ao_1')?>

    <?php // echo $form->field($model, 'slot_ao_1')?>

    <?php // echo $form->field($model, 'status_id')?>

    <?php // echo $form->field($model, 'updated_at')?>

    <?php // echo $form->field($model, 'created_at')?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
