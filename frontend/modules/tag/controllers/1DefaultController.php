<?php

namespace frontend\modules\tag\controllers;

use yii\web\Controller;
use yii\data\ActiveDataProvider;
use common\models\Tag;

/**
 * Default controller for the `tag` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $query = Tag::find()->where('net_id' > 0);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 40,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
