<?php

namespace frontend\modules\ucn\controllers;

use yii\web\Controller;
use common\models\Net;
use common\models\Ucn;
use common\models\Pm;
use common\models\Node;

/**
 * Default controller for the `ucn` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $nets = Net::find()->all();
        $ucns = Ucn::find()->all();
        return $this->render('index', [
            'nets' => $nets,
            'ucns' => $ucns
        ]);
    }

    /**
     * Renders the  view for the all ucns on the net
     * @return string
     */
    public function actionShow($net_id)
    {
        $net = Net::findNet($net_id);

        $ucns = Ucn::find(['net_id' => $net_id])->all();
        $nodes = Node::find()->where(['net_id' => $net_id])->andWhere('ucn_id IS NOT NULL')->all();
        $pms = Pm::find(['net_id' => $net_id])->all();
        return $this->render('show', [
            'net' => $net,
            'ucns' => $ucns,
            'nodes' => $nodes,
            'pms' => $pms,
        ]);
    }

    /**
     * Renders the  view for the single ucn
     * @return string
     */
    public function actionView($net_id, $ucn_id)
    {
        $net = Net::findNet($net_id);
        $ucn = Ucn::findUcn($net_id, $ucn_id);
        $nodes = Ucn::findNodes($net_id, $ucn_id);
        $pms = Pm::findPms($net_id, $ucn_id);

        return $this->render('view', [
            'net' => $net,
            'ucn' => $ucn,
            'nodes' => $nodes,
            'pms' => $pms,
        ]);
    }
}
