<?php

/**
 * @var $this use yii\web\View;
 * @var $net common\models\Net
 */
use yii\helpers\Html;
use yii\helpers\Url;

if (!isset($this->breadcrumbs)) {
    $this->params['breadcrumbs'][] = ['label' => 'Net', 'url' => ['/net/']];
    $this->params['breadcrumbs'][] = ['label' => 'UCN', 'url' => ['/ucn']];
    $this->params['breadcrumbs'][] = ['label' => Html::encode($ucn[0]->ucn_id)];
}

?>
<div class="container">

    <h1>View Ucn
        <?php echo Html::encode($ucn[0]->ucn_id); ?>
    </h1>
    <div class="row">
        <div class="col-mid-12">
            <strong>Net : </strong>:
            <?php echo Html::encode($ucn[0]->net_id); ?>
        </div>


        <div class="row">
            <div class="col-lg-12">

                <?php foreach ($nodes as $node) : ?>
                <div class="card text-center" style="width: 18rem; text-align:center;display:inline-block;">
                    <div class="card-header">
                        <h4 class="card-title"><a href="<?php echo Url::to(['/net/' . $net->id . '/' . $node->node_num]); ?>">
                                <?php echo Html::encode($node->getTypeName($node->type_id) . ' ' . $node->node_num); ?>
                            </a></h4>

                    </div>
                    <div class="card-body">

                        <p class="card-text">
                            <?php echo Html::encode($node->getTypeDescription($node->type_id)); ?>
                        </p>
                        <hr>
                    </div>



                </div>


                <?php endforeach; ?>

            </div>


        </div>



    </div>

    <div class="col-lg-12">

        <?php foreach ($pms as $pm) : ?>
        <div class="card text-center" style="width: 18rem; text-align:center;display:inline-block;">
            <div class="card-header">
                <h4 class="card-title"><a href="<?php echo Url::to(['/pm/' . $net->id . '-' . $pm->ucn_id . '-' . $pm->pm_num]); ?>">
                        <?php echo Html::encode($pm->type . ' ' . $pm->pm_num); ?>
                    </a></h4>
            </div>
            <div class="card-body">

                <p class="card-text">
                    <?php echo Html::encode($pm->type); ?>
                </p>
                <hr>
            </div>
        </div>
        <?php endforeach; ?>

    </div>


</div>

</div>