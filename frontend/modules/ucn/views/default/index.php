<?php
/**
 * @var $this use yii\web\View;
 * @var $nets common\models\Net
 */
use yii\bootstrap\Tabs;

$this->title = 'Ucn';
if (!isset($this->breadcrumbs)) {
    $this->params['breadcrumbs'][] = ['label' => 'Net', 'url' => ['/net/']];
    $this->params['breadcrumbs'][] = ['label' => 'UCN'];
}

?>


<div class="lcn-single-view">
    <h1> UCN  View:</h1>
    <div class="row">
        <div class="body-content">

            <?php
            foreach ($nets as $net) {
                $tabItems[] = [
                    'label' => $net->type . ' ' . $net->name,
                    'content' => $this->render('show', [
                        'net' => $net,
                        'nodes' => $net->getNims($net->id),
                    ]),
                ];
            }

            echo Tabs::widget([
                'options' => ['tag' => 'div'],
                'itemOptions' => ['tag' => 'div'],
                'items' => $tabItems,
            ]);
            ?>


<?php
$script = <<< JS
$(function() {
//save the latest tab (http://stackoverflow.com/a/18845441)
$('a[data-toggle="tab"]').on('click', function (e) {
localStorage.setItem('lastTab', $(e.target).attr('href'));
});

//go to the latest tab, if it exists:
var lastTab = localStorage.getItem('lastTab');

if (lastTab) {
    $('a[href="'+lastTab+'"]').click();
    } 
});
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>

    </div>
</div>
   

