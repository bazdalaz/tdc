<?php
/**
 * @var $this use yii\web\View;
 * @var $node common\models\Node;
 */
use yii\helpers\Html;
use yii\helpers\Url;

?>




<div class="container">
    <h1>LCN
        <?php echo Html::encode($net->id); ?> UCN Nodes:
    </h1>
    <div class="row">
        <div class="col-sm-12">
            <?php foreach ($nodes as $node) : ?>
            <div class="card card-hover card-outline-info text-center" style="width: 26rem; text-align:center;display:inline-block;">
                <h3 class="card-title">
                    <?php echo Html::encode($node->getTypeName($node->type_id)); ?>
                    <?php echo Html::encode($node->node_num); ?>
                </h3>

                <div class="card-body">

                    <p class="card-text"> <strong>Net : </strong>
                        <?php echo Html::encode($node->net_id); ?>
                    </p>

                    <p class="card-text">
                        <?php if (isset($node->ucn_id)) : ?>
                        <a href="<?php echo Url::to(['/net/' . $node->net_id . '/ucn/' . $node->ucn_id]);  ?>"
                            class="btn btn-link"><strong>UCN :
                                <?php echo Html::encode($node->ucn_id); ?></strong>
                        </a>
                        <?php else : ?>
                        Not Applicable for
                        <?php echo Html::encode($node->getTypeName($node->type_id)); ?>
                        <?php endif; ?>
                    </p>

                    <p class="card-text">
                        <?php echo Html::encode($node->getTypeDescription($node->type_id)); ?>
                    </p>

                    <p class="card-text"> <strong>Plant : </strong>
                        <?php echo Html::encode($node->getPlantName()); ?>

                    </p>

                    <p class="card-text"> <strong>PM : </strong>
                        <?php $pms = $node->getPms();?>
                        <?php if (is_array($pms)) : ?>

                        <?php foreach ($pms as $pm) :?>

                        <a href="<?php echo Url::to(['/pm/' . $pm->net_id . '-' . $pm->ucn_id . '-' . $pm->pm_num]);  ?>"
                            class="btn btn-link"><strong>
                                <?php echo Html::encode($pm->pm_num); ?></strong>
                        </a>

                        <?php endforeach;?>
                        <?php else : ?>
                         ..
                        <?php endif; ?>


                    </p>

                </div>
            </div>
            <?php endforeach;?>

        </div>
    </div>
</div>