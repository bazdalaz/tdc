<?php

namespace frontend\modules\node\controllers;

use yii\web\Controller;
use common\models\Node;

/**
 * Default controller for the `node` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionView($id, $node_num)
    {
        return $this->render('view', [
            'node' => Node::findNode($id, $node_num),
        ]);
    }
}
