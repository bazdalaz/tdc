<?php
/**
 * @var $this use yii\web\View;
 * @var $node common\models\Node;
 */
use yii\helpers\Html;

$this->title = 'Node ' . Html::encode($node->node_num);
$this->params['breadcrumbs'][] = ['label' => 'Net', 'url' => ['/net/']];
$this->params['breadcrumbs'][] = ['label' => Html::encode($node->getTypeName($node->type_id)) . $node->node_num];
?>

<div class="lcn-single-view">
    <h1>LCN 
        <?php echo Html::encode($node->net_id); ?>
    </h1>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h2 class="card-title">View Node
                        <?php echo Html::encode($node->node_num); ?>
                    </h2>

                    <p class="card-text"> <strong>Net : </strong>
                        <?php echo Html::encode($node->net_id); ?>
                    </p>

                    <p class="card-text"> <strong>Type : </strong>
                        <?php echo Html::encode($node->getTypeDescription($node->type_id)); ?>
                        (<?php echo Html::encode($node->getTypeName($node->type_id)); ?>)
                    </p>

                    <p class="card-text"> <strong>Plant : </strong>
                      
                        <?php echo Html::encode($node->getPlantName()); ?>
                      
                    </p>

                    <p class="card-text"> <strong>Area : </strong>
                        <?php if (isset($node->area_id)) : ?>
                        <?php echo Html::encode($node->area_id); ?>
                        <?php else : ?>
                        Not Applicable for
                        <?php echo Html::encode($node->getTypeName($node->type_id)); ?>
                        <?php endif; ?>
                    </p>

                    <p class="card-text"> <strong>Console : </strong>
                        <?php if (isset($node->area_id)) : ?>
                        <?php echo Html::encode($node->console_id); ?>
                        <?php else : ?>
                        Not Applicable for
                        <?php echo Html::encode($node->getTypeName($node->type_id)); ?>
                        <?php endif; ?>
                    </p>

                    <p class="card-text"> <strong>Status : </strong>
                        <?php if (isset($node->unit_id)) : ?>
                        <?php echo Html::encode($node->unit_id); ?>
                        <?php else : ?>
                        Not updated on this 
                        <?php echo Html::encode($node->getTypeName($node->type_id)); ?>
                        <?php endif; ?>
                    </p>




                    <a href="<?php echo Yii::$app->request->referrer ?>"
                        class="btn btn-primary">Go Back</a>
                </div>
            </div>
        </div>
    </div>
</div>