<?php
/**
 * @var $this use yii\web\View;
 * @var $node common\models\Node;
 *
 */
use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\grid\GridView;

$this->title = 'Jb';
$this->params['breadcrumbs'][] = $this->title ;

?>



<div class="jb-default-index">


    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <?php Pjax::begin();?>

                <?php echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'responsive' => false,
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                    ],
                    // 'perfectScrollbar' => true,
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => 'JB', 'options' => ['colspan' => 4, 'class' => 'text-center secondary']],
                                ['content' => 'Location', 'options' => ['colspan' => 4, 'class' => 'text-center success']],
                                ['content' => '', 'options' => ['colspan' => 1, 'class' => 'text-center secondary']],
                                ['content' => 'Actions', 'options' => ['colspan' => 1, 'class' => 'text-center success']],
                            ],
                            // 'options' => ['class' => 'skip-export'] // remove this row from export
                        ],
                    ],
                    'resizableColumns' => true,

                    'toolbar' => [
                        [
                            'content' => Html::button('<i class="glyphicon glyphicon-plus"></i>', [
                                'type' => 'button',
                                'onclick' => "window.location=(['/jb/create'])",
                                'title' => Yii::t('kvgrid', 'Add JB'),
                                'class' => 'btn btn-success',
                                //  'class'=>'btn btn-success'
                            ]) . ' ' .
                                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['/jb/'], [
                                    'class' => 'btn btn-outline-primary',
                                    'title' => Yii::t('kvgrid', 'Reset Grid')
                                ]),
                        ],
                        '{toggleData}',
                        'toggleDataContainer' => ['class' => 'btn-group-sm'],
                        '{export}',
                        'exportContainer' => ['class' => 'btn-group-sm'],
                    ],

                    // set export properties
                    'export' => [
                        //'fontAwesome' => true
                        'hiddenColumns' => [0, 9], // SerialColumn & ActionColumn
                        'disabledColumns' => [1, 2], // ID & Name
                        'dropdownOptions' => [
                            'label' => 'Export All',
                            'class' => 'btn btn-secondary'
                        ]
                    ],
                    // parameters
                    'bordered' => true,
                    'floatHeader' => true,
                    'floatHeaderOptions' => ['scrollingTop' => '50'],
                    'striped' => true,
                    'condensed' => false,
                    'responsive' => false,
                    'hover' => true,
                    'showPageSummary' => false,
                    'panel' => [
                        'type' => GridView::TYPE_SUCCESS,
                        'heading' => true,
                        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i>&nbsp JBs</h3>',
                    ],
                    'persistResize' => false,
                    'toggleDataOptions' => ['minCount' => 32],
                    // 'exportConfig' => $exportConfig,
                    'itemLabelSingle' => 'jb',
                    'itemLabelPlural' => 'jbs',
                    'columns' => [
                        [
                            'attribute' => 'plant',
                            'headerOptions' => ['style' => 'width:8%'],
                            'filter' => common\models\Plant::getPlantList(),
                        ],
                        [
                            'attribute' => 'type_id',
                            'headerOptions' => ['style' => 'width:5%'],
                            'value' => function ($model) {
                                return $model->type->type_name;
                            },
                            'label' => 'Type',
                            'headerOptions' => ['style' => 'width:15%'],
                            'filter' => common\models\JbType::getTypeList(),
                        ],
                        [
                            'label' => 'JB Name',
                            'attribute' => 'jb_num',
                            'value' => function ($model) {
                                return $model->plant . '-' . $model->type->short_name . '-' . $model->jb_num;
                            },
                        ],
                        [
                            'label' => 'Cab',
                            'headerOptions' => ['style' => 'width:5%'],
                            'attribute' => 'jb_cab',
                        ],

                        [
                            'label' => 'Elevation',
                            'headerOptions' => ['style' => 'width:5%'],
                            'attribute' => 'elevation',
                        ],

                        [
                            'label' => 'V',
                            'headerOptions' => ['style' => 'width:5%'],
                            'attribute' => 'v',
                        ],
                        [
                            'label' => 'H',
                            'headerOptions' => ['style' => 'width:5%'],
                            'attribute' => 'h',
                        ],
                        [
                            'label' => 'Location',
                            'attribute' => 'location',
                        ],
                        [
                            'label' => 'Notes',
                            'headerOptions' => ['style' => 'width:15%'],
                            'attribute' => 'notes',
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            // you may configure additional properties here
                        ],
                    ],
                ]) ?>
                <?php Pjax::end();?>





            </div>


        </div>



    </div>

</div>