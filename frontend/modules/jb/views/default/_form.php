<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Jb */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jb-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jb_num')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jb_cab')->textInput() ?>

    <?= $form->field($model, 'plant')->dropdownList(\common\models\Plant::getPlantList()); ?>

    <?= $form->field($model, 'type_id')->dropdownList([\common\models\JbType::getTypeList()]); ?>

    <?= $form->field($model, 'elevation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'v')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'h')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'location')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>