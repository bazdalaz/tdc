<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Jb */

$this->title = $model->plant . '-' . $model->type->short_name . '-' . $model->jb_num;
$this->params['breadcrumbs'][] = ['label' => 'Jbs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jb-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
