<?php

namespace frontend\modules\plant\controllers;

use yii\web\Controller;
use common\models\Plant;

/**
 * Default controller for the `plant` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'plants' => Plant::find()->all(),
        ]);
    }

    /**
     * Renders the single view for the module
     * @return string
     */
    public function actionView($plant_name)
    {
        $plant = Plant::findOne(['name' => $plant_name]);
        $ucn = $plant->ucn;
        $nims = $ucn->nodes;

        return $this->render('view', [
            'plant' => $plant,
            'ucn' => $plant->ucn,
            'nims' => $nims,
        ]);
    }
}
