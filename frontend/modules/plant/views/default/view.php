<?php
/**
 * @var $this use yii\web\View;
 * @var $node common\models\Node;
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Tabs;

$this->title = 'Plant ' . Html::encode($plant->name);
$this->params['breadcrumbs'][] = ['label' => 'Plants', 'url' => ['/plant/']];
$this->params['breadcrumbs'][] = ['label' => $plant->name];
?>

<div class="lcn-single-view">
    <h2>LCN
        <?php echo Html::encode($plant->getNetName());?> Plant:
        <?php echo Html::encode($plant->name);?>
    </h2>
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <div class="card">
                    <div class="card-body">


                        <p class="card-text"> <strong>Area : </strong>
                            <?php echo Html::encode($plant->area_id); ?>

                            <p class="card-text"> <strong>Console : </strong>
                                <?php echo Html::encode($plant->console_id); ?>
                            </p>

                            <p class="card-text"> <strong>Letter :
                                    <?php echo Html::encode($plant->letter); ?></strong>
                            </p>


                            <p class="card-text">
                                <a href="<?php echo Url::to(['/net/' . $plant->net_id . '/ucn/' . $plant->ucn_id]); ?>">
                                    <strong>UCN : </strong>
                                    <?php echo Html::encode($plant->ucn_id); ?>
                                </a>


                                <p class="card-text"> <strong>NIMs : </strong>
                                    <?php foreach ($nims as $nim) :?>
                                    <a href="<?php echo Url::to(['/net/' . $nim->net_id . '/' . $nim->node_num]); ?>">
                                        <?php echo Html::encode($nim->getTypeName($nim->type_id) . ' ' . $nim->node_num); ?>
                                    </a>

                                    <?php endforeach;?>

                                </p>


                                <a href="<?php echo Yii::$app->request->referrer ?>"
                                    class="btn btn-primary">Go Back</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-5">
                <div class="card">
                    <div class="card-body">


                        <p class="card-text"> <strong>Sequences: : </strong>
                            <?php echo Html::encode('???'); ?>

                            <p class="card-text"> <strong>Loaded : </strong>
                                <?php echo Html::encode('???'); ?>
                            </p>

                            <p class="card-text"> <strong>Running :
                                    <?php echo Html::encode('???'); ?></strong>
                            </p>

                            <a href="<?php echo Url::to(['/' . $plant->name . '/seq']); ?>"
                                class="btn btn-success">Code Base</a>
                    </div>
                </div>
            </div>
        </div>

<br>
<hr>


    </div>

    <div class="contaier">

        <?php
            foreach ($plant->pms as $pm) {
                if (isset($pm)) {
                    $label = 'PM' . $pm->pm_num;
                } else {
                    $label = 'PM not exist';
                }
                $tabItems[] = [
                    'label' => $label,
                    'content' => $this->render('tab_view', [
                        'pm' => $pm,
                    ]),
                    'options' => ['style' => 'background-color: navy'],
                ];
            }

                echo Tabs::widget([
                    'options' => ['tag' => 'div'],
                    'itemOptions' => ['tag' => 'div'],
                    'items' => $tabItems,
                ]);
        ?>
    </div>
    
</div>