<?php

/**
 * @var $this use yii\web\View;
 * @var $net common\models\Net
 */
use Yii;
use yii\helpers\Url;
use yii\helpers\Html;

?>
<div class="container">

    <div class="row">
        <div class="col-lg-12">

            <div class="card " style="text-align:center;display:inline-block;">
                <div class="card-header text-center">
                        <h4 class="card-title">
                        <strong><?php echo Html::encode($pm->type);?>
                <?php echo Html::encode($pm->pm_num); ?> </strong>

            <a href="<?php echo Url::to(['/pm/' . $pm->net_id . '-' . $pm->ucn_id . '-' . $pm->pm_num]); ?>">

                <?php echo Html::encode(':    Overview;   '); ?>
            </a>
            &nbsp;
            <a href="<?php echo Url::to(['/pm/' . $pm->net_id . '-' . $pm->ucn_id . '-' . $pm->pm_num . '/layout/']); ?>">
                <?php echo Html::encode('    Layout'); ?>
            </a>
                        </h4>

                    </div>
                <div class="card-body">

                    <p class="card-text"><strong>Requlatory PV Points:</strong>
                        <?php echo Html::encode($pm->usedRpvCount());?>
                        /
                        <?php echo Html::encode($pm->rpv_cnt);?>
                        &nbsp <strong>Regulatory Control Points:</strong>
                        <?php echo Html::encode($pm->usedRcCount());?>
                        /
                        <?php echo Html::encode($pm->rc_cnt);?>
                        &nbsp <strong>Digital Composite Points:</strong>
                        <?php echo Html::encode($pm->usedDcCount());?>
                        /
                        <?php echo Html::encode($pm->dc_cnt);?>
                        &nbsp <strong>Process Module Points:</strong>
                        <?php echo Html::encode($pm->usedPrModCount());?>
                        /
                        <?php echo Html::encode($pm->seq_cnt);?>
                        &nbsp <strong>Analog Input Points:</strong>
                        <?php echo Html::encode($pm->usedAiCount());?>
                        /
                        <?php echo Html::encode($pm->aiCount());?>
                        &nbsp <strong>Digital Input Points:</strong>
                        <?php echo Html::encode($pm->usedDiCount($pm->pm_num));?>
                        /
                        <?php echo Html::encode($pm->diCount($pm));?>
                        &nbsp <strong>Digital Output Points:</strong>
                        <?php echo Html::encode($pm->usedDoCount());?>
                        /
                        <?php echo Html::encode($pm->doCount());?>
                        &nbsp <strong>Analog Output Points:</strong>
                        <?php echo Html::encode($pm->usedAoCount());?>
                        /
                        <?php echo Html::encode($pm->aoCount());?>
                    </p>

                    <p class="card-text"><strong>Times:</strong>
                        <?php echo Html::encode($pm->time_cnt . ';');?>
                        &nbsp <strong>Arrays:</strong>
                        <?php echo Html::encode($pm->arr_cnt) . ';';?>
                        &nbsp <strong>Scan Period:</strong>
                        <?php echo Html::encode($pm->scan_per);?>
                    </p>
                </div>
            </div>


        </div>
    </div>
</div>