<?php
/**
 * @var $this use yii\web\View;
 * @var $node common\models\Node;
 */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Plants';
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>



<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <h1>LCN Plants:</h1>
        </div>
    </div>


    <hr>
    <div class="row">
        <div class="col-lg-12">
            <?php foreach ($plants as $plant) : ?>
            <?php $color = ($plant->net_id > 1) ? 'bg-info' : 'bg-success';?>
            <div class="card card-hover card-secondary mb-4" style="width: 25rem; height:25rem; text-align:center;display:inline-block;">
                <div class="card-header " >
                    <h4 class="card-title  <?php echo $color ?>">
                        <a href="<?php echo Url::to(['/plant/' . $plant->name]);  ?>"
                            class="btn btn-secondary"> <strong> LCN
                                <?php echo Html::encode($plant->getNetName());?>
                                Plant:
                                <?php echo Html::encode($plant->name);?></strong>
                        </a>
                    </h4>
                </div>

                <div class="card-body">
                    <div class="card-text">Area :
                        <?php echo Html::encode($plant->area_id); ?>
                        Console :
                        <?php echo Html::encode($plant->console_id); ?>

                        <br>Letter :
                        <?php echo Html::encode($plant->letter) ; ?>
                        </>
                    </div>
                    <hr>

                    <p class="card-text"> <strong>NIMs : </strong>
                        <?php foreach ($plant->nims as $nim) :?>
                        <a href="<?php echo Url::to(['/net/' . $nim->net_id . '/' . $nim->node_num]); ?>">
                            <?php echo Html::encode($nim->getTypeName($nim->type_id) . ' ' . $nim->node_num); ?>
                        </a>

                        <?php endforeach;?>

                    </p>

                    <hr>
                    <p class="card-text"> <strong>PMs : </strong>
                        <?php foreach ($plant->pms as $pm) :?>
                        <a href="<?php echo Url::to(['/pm/' . $pm->net_id . '-' . $plant->ucn->ucn_id . '-' . $pm->pm_num]); ?>">
                            <?php echo Html::encode($pm->pm_num . ' '); ?>
                        </a>

                        <?php endforeach;?>

                    </p>


                </div>
            </div>

            <?php endforeach;?>
        </div>
    </div>
</div>