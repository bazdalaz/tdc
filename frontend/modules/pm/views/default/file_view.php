<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\Tag;

?>

<div class="flex-row">
    <?php
                    foreach ($file as $card) {
                        echo '<div class="col-sm-1" >';

                        switch ($card->card_type) {
                            case 1:
                            case 2:
                                $class = 'class=" layout-header bg-success"';
                                break;
                            case 3:
                                $class = 'class=" layout-header bg-info"';
                                break;

                            case 4:
                            case 5:
                            $class = 'class=" layout-header bg-primary"';
                            break;

                            case 6:
                            case 7:
                            $class = 'class=" layout-header bg-danger"';
                            break;

                            default:
                            $class = 'class=" layout-header bg-warning"';
                                break;
                        }

                        echo "<table class='table-striped table-hover table-bordered table-condensed'>
                        <caption  {$class}> Slot {$card->card_slot} <br> Module {$card->card_num} </caption> 
                        <tr>
                        <th>#</th>
                        
                        <th> {$card->type->type_name}</th>
                        </tr>";

                        foreach ($card->slots as $slot => $point) {
                            $point_class = Tag::getClassByStatus($point);

                            if (ArrayHelper::getValue($point, $slot)) {
                                $url = Html::a(ArrayHelper::getValue($point, $slot), '/tag/view/' . ArrayHelper::getValue($point, $slot));
                            } else {
                                $url = Html::a('create', '/tag/create/' . $plant. '/' .
                                                                                $card->pm_num . '/' .
                                                                                $card->card_num . '/' .
                                                                                $slot . '/' .
                                                                                $card->net_id . '/' .
                                                                                $card->ucn_id . '/' .
                                                                                $card->type->type_id);
                            }

                            echo '<tr> ';
                            echo '<td >' . $slot . '</td>';
                            echo '<td ' . $point_class . '>' . $url . '</td>';

                            echo '</tr>';
                        }
                        echo '</table>';
                        echo '</div>';
                    }

                    ?>

</div>