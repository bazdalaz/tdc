<?php
/**
 * @var $this use yii\web\View;
 * @var $node common\models\Node;
 */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $net->getNetName() . ' PMs';
$this->params['breadcrumbs'][] = ['label' => 'Net ' . $net->getNetName(), 'url' => ['/net/']];
$this->params['breadcrumbs'][] = 'Process Modules';

?>



<div class="lcn-single-view">
    <h1>LCN
        <?php echo Html::encode($net->name); ?>
        Process Modules:
    </h1>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">



                <?php foreach ($pms as $pm) : ?>
                <div class="card card-hover card-outline-primary card-secondary mb-3" style="width: 18rem; text-align:center;display:inline-block;"">
                        <div class="
                    card-header">
                    <h4 class="card-title"><a href="<?php echo Url::to(['/pm/' . $net->id . '-' . $pm->ucn_id . '-' . $pm->pm_num]); ?>">
                            <?php echo Html::encode($pm->type . ' ' . $pm->pm_num); ?>
                        </a></h4>
                </div>


                <div>

                    <p class="card-text"> <strong>UCN : </strong>
                        <?php echo Html::encode($pm->ucn_id); ?>
                    </p>

                    <p class="card-text"> <strong>Type : </strong>
                        <?php echo Html::encode($pm->type); ?>
                    </p>

                    <p class="card-text"> <strong>Plant : </strong>
                        <?php echo Html::encode($pm->getPlant()->name); ?>
                    </p>

                </div>
            </div>
            <?php endforeach;?>
        </div>
    </div>
</div>