<?php

/**
 * @var $this use yii\web\View;
 * @var $net common\models\Net
 */

//use Yii;
use yii\helpers\Html;
use yii\bootstrap\Tabs;

$this->title = $pm->plant . ' PM' . Html::encode($pm->pm_num);
$this->params['breadcrumbs'][] = ['label' => 'LCN ' . $pm->getNetName(), 'url' => ['/net/']];
$this->params['breadcrumbs'][] = ['label' => 'UCN ' . $pm->ucn_id, 'url' => ['/ucn/']];
$this->params['breadcrumbs'][] = ['label' => 'Plant  ' . $pm->plant, 'url' => ['/plant/' . $pm->plant]];
$this->params['breadcrumbs'][] = Html::encode($pm->type) . ' ' . Html::encode($pm->pm_num);
?>

<div class="contaier-fluid layout">

    <?php
    foreach ($files as $file) {
        if (isset($file[0])) {
            $label = 'FILE' . $file[0]->card_file;
        } else {
            $label = '';
        }
        $tabItems[] = [
            'label' => $label,
            'content' => $this->render('file_view', [
                'file' => $file,
                'plant' => $pm->plant,
            ]),
        ];
    }

    echo Tabs::widget([
        'options' => ['tag' => 'div'],
        'itemOptions' => ['tag' => 'div'],
        'items' => $tabItems,
    ]);
    ?>

    <?php
    $script = <<< JS
$(function() {
//save the latest tab (http://stackoverflow.com/a/18845441)
$('a[data-toggle="tab"]').on('click', function (e) {
localStorage.setItem('lastTab', $(e.target).attr('href'));
});

//go to the latest tab, if it exists:
var lastTab = localStorage.getItem('lastTab');

if (lastTab) {
    $('a[href="'+lastTab+'"]').click();
    } 
});
JS;
    $this->registerJs($script, yii\web\View::POS_END);
    ?>



</div>