<?php

namespace frontend\modules\pm\controllers;

use yii\web\Controller;
use common\models\Pm;
use common\models\Net;
use common\models\Card;
use yii\data\ActiveDataProvider;

/**
 * Default controller for the `pm` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($net_id)
    {
        return $this->render('index', [
            'pms' => Net::findOne($net_id)->pms,
            'net' => Net::findNet($net_id)
        ]);
    }

    /**
     * Renders the single pm view
     * @return string
     */
    public function actionView($pm_id)
    {
        $pm = Pm::getPmById($pm_id);
        $dataProvider = new ActiveDataProvider([
            'query' => Card::find()->where(['net_id' => $pm->net_id, 'ucn_id' => $pm->ucn_id, 'pm_num' => $pm->pm_num]),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('view', [
            'pm' => $pm,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLayout($pm_id)
    {
        $pm = Pm::getPmById($pm_id);
        $file1 = Card::findAll(['net_id' => $pm->net_id, 'ucn_id' => $pm->ucn_id, 'pm_num' => $pm->pm_num, 'card_file' => 1]);
        $file2 = Card::findAll(['net_id' => $pm->net_id, 'ucn_id' => $pm->ucn_id, 'pm_num' => $pm->pm_num, 'card_file' => 2]);
        $file3 = Card::findAll(['net_id' => $pm->net_id, 'ucn_id' => $pm->ucn_id, 'pm_num' => $pm->pm_num, 'card_file' => 3]);
        $file4 = Card::findAll(['net_id' => $pm->net_id, 'ucn_id' => $pm->ucn_id, 'pm_num' => $pm->pm_num, 'card_file' => 4]);

        //$this->layout = 'grid_layout';
        return $this->render('layout', [
            'pm' => $pm,
            'files' => [$file1, $file2, $file3, $file4],
        ]);
    }
}
