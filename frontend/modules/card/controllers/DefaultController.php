<?php

namespace frontend\modules\card\controllers;

use yii\web\Controller;
use common\models\Card;

/**
 * Default controller for the `card` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    // public function actionIndex($pm_id)
    // {
    //     $pm= Pm::getPmByd($pm_id);
    //     $pm= $pm->cards;
    //     return $this->render('index', [
    //         'cards' => $pm,
    //         'cards' => $cards,
    //     ]);
    // }

    /**
     * Renders the  view for the single card
     * @return string
     */
    public function actionView($pm_id, $card_num)
    {
        $card = Card::findCard($pm_id, $card_num);

        $card_type = $card->type;

        return $this->render('view', [
            'card' => $card,
            'pm' => $card->pm,
        ]);
    }
}
