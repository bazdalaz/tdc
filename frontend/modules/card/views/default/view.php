<?php

/**
 * @var $this use yii\web\View;
 * @var $card common\models\Card
 */
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = $pm->plant . 'PM ' . $pm->pm_num . ' Card ' . Html::encode($card->card_num);
$this->params['breadcrumbs'][] = ['label' => 'LCN ' . $pm->getNetName(), 'url' => ['/net/']];
$this->params['breadcrumbs'][] = ['label' => 'UCN ' . $pm->ucn_id, 'url' => ['/ucn/']];
$this->params['breadcrumbs'][] = ['label' => 'Plant  ' . $pm->plant, 'url' => ['/plant/' . $pm->plant]];
$this->params['breadcrumbs'][] = ['label' => 'PM  ' . $pm->pm_num, 'url' => ['/pm/' . $pm->net_id . '-' . $pm->ucn_id . '-' . $pm->pm_num]];
$this->params['breadcrumbs'][] = 'Card ' . Html::encode($card->card_num);
?>


<div class="container">

    <div class="row">
        <div class="col-lg-12">

            <div class="card " style="width:110rem;text-align:center;display:inline-block;">
                <div class="card-header text-center">
                    <h4 class="card-title">
                        <?php echo Html::encode($card->card_num . '  -  ' . $card->type->type_alias);?>
                    </h4>

                </div>
                <div class="card-body">

                    <p class="card-text"><strong>
                            <?php echo Html::encode($card->type->type_name);?></strong>
                        &nbsp <strong>Points Count:</strong>
                        <?php echo Html::encode($card->countUsedSlots());?>
                        /
                        <?php echo Html::encode($card->slot_count);?>
                    </p>

                    <p class="card-text"><strong>Description:</strong>

                        <?php echo Html::encode($card->type->description);?>
                    </p>

                </div>

            </div>
            <br><br>
            <hr>

            <h4>Points: </h4>

            <?php
                    echo "<table class='table table-striped table-hover' border='1'>
                    <tr>
                    <th>Slot</th>
                    <th> &nbsp{$card->type->type_name}  Points</th>
                    </tr>";

                    foreach ($card->slots as $slot => $point) {
                        echo '<tr>';
                        echo '<td>' . $slot . '</td>';
                        echo '<td>' . Html::a(ArrayHelper::getValue($point, $slot), '/tag/view/' . ArrayHelper::getValue($point, $slot)) . '</td>';

                        echo '</tr>';
                    }
                    echo '</table>';

                    ?>

        </div>
    </div>
</div>