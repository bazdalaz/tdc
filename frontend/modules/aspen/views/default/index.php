<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="tag-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tag')->textInput(['maxlength' => 16]) ?>

    <?= $form->field($model, 'map')->dropdownList([
        ['ip_analogdef' => 'ip_analogdef',['ip_textdef' => 'ip_textdef'],['ip_discretedef'=> 'ip_discretedef']],
    ]);
     ?>
    <div class="form-group">
        <?= Html::submitButton('Go', ['class' => 'btn btn-success']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
