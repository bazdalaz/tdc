<?php

namespace frontend\modules\aspen\forms;

use yii\base\Model;

/**
 * Password reset form
 */
class AspenForm extends Model
{
    public $tag;
    public $map;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tag', 'map'], 'required'],
        ];
    }
}
