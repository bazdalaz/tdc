<?php

namespace frontend\modules\aspen\services;

use Yii;
use \SimpleXMLElement;

class SoapService
{
    public function aspenGetTag($tag,$map)
    {
        $wsdl = Yii::getAlias('@ip21Wdsl');

        $client = new \SoapClient($wsdl, [
            'exrentions' => 1,
            'cache_wsdl' => WSDL_CACHE_MEMORY,
            'trace' => true,
        ]);

        // $map = 'ip_analogdef';
        // $tag = 'gwi%';
        $result = $client->ExecuteSQL([
            'command' => "Select * from $map where name like '$tag'",
        ]);

        $data = new SimpleXMLElement($result->ExecuteSQLResult);

        return [$data];
    }
}
