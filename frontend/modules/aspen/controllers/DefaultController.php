<?php

namespace frontend\modules\aspen\controllers;

use Yii;
use yii\web\Controller;
use frontend\modules\aspen\services\SoapService;
use frontend\modules\aspen\forms\AspenForm;

/**
 * Default controller for the `aspen` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $model = new AspenForm();


        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            return $this->redirect(['view', 'tag' => $model->tag, 'map'=>$model->map]);
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionView($tag, $map)
     {


        $data = (new SoapService())->aspenGetTag($tag, $map);


        return $this->render('view', [
            'data' => $data,
        ]);
    }
}
