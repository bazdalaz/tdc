<?php

namespace frontend\modules\reports\controllers;

use yii\web\Controller;
use frontend\modules\reports\entities\AccessReports;
use frontend\modules\reports\entities\CodeReports;
use frontend\modules\reports\entities\LcnReports;
use frontend\modules\reports\entities\TagReports;

/**
 * Default controller for the `reports` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $accessReports = new AccessReports();
        $codeReports = new CodeReports();
        $lcnReports = new LcnReports();
        $tagReports = new TagReports();
        $reports = [$accessReports, $codeReports, $lcnReports, $tagReports];

        return $this->render('index', [
            'reports' => $reports,
        ]);
    }
}
