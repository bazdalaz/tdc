<?php
namespace frontend\modules\reports\entities;

use common\models\Plant;
use common\models\Pm;
use common\models\Tag;


class TagReports 
{

    public static function checkUniqueTags($pm)
    {
        $tags = Tag::findAll(['net_id' => $pm->net_id, 'ucn_id' => $pm->ucn_id, 'pm_num'=>$pm->pm_num]);
    
        return $tags;
    }


} 