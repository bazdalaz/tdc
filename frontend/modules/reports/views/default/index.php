<?php
/**
 * @var $this use yii\web\View;
 */
use yii\bootstrap\Tabs;

$this->title = 'Reports';
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>



<div class="container">

    <div class="row">
        <div class="col-lg-12">

            <?php
                $tabItems[0] = [
                    'label' => 'Access',
                    'content' => $this->render('access_view', [
                        'reportContent' => $reports[0],
                    ]),
                ];
                $tabItems[1] = [
                    'label' => 'Code',
                    'content' => $this->render('code_view', [
                        'reportContent' => $reports[1],
                    ]),
                ];
                $tabItems[2] = [
                    'label' => 'LCN',
                    'content' => $this->render('lcn_view', [
                        'reportContent' => $reports[2],
                    ]),
                ];
                $tabItems[3] = [
                    'label' => 'Tag',
                    'content' => $this->render('tag_view', [
                        'reportContent' => $reports[3],
                    ]),
               ];

            echo Tabs::widget([
                'options' => ['tag' => 'div'],
                'itemOptions' => ['tag' => 'div'],
                'items' => $tabItems,
            ]);
            ?>

<?php
    $script = <<< JS
$(function() {
//save the latest tab (http://stackoverflow.com/a/18845441)
$('a[data-toggle="tab"]').on('click', function (e) {
localStorage.setItem('lastTab', $(e.target).attr('href'));
});

//go to the latest tab, if it exists:
var lastTab = localStorage.getItem('lastTab');

if (lastTab) {
    $('a[href="'+lastTab+'"]').click();
    } 
});
JS;
    $this->registerJs($script, yii\web\View::POS_END);
    ?>


        </div>



    </div>
</div>
</div>