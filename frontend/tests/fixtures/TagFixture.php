<?php

namespace frontend\tests\fixtures;

use yii\test\ActiveFixture;

class TagFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Tag';
}
