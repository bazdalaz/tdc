<?php

namespace frontend\tests;

use frontend\tests\fixtures\TagFixture;

class TagTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    public function _fixtures()
    {
        return ['tags' => TagFixture::className()];
    }

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testSomeFeature()
    {
    }
}
