#!/bin/bash

BU_PATH="$HOME/backups"
YEAR=$(date +'%Y')
MONTH=$(date +'%m')
DAY=$(date +'%d')
HOUR=$(date +'%H')
MIN=$(date +'%'M)

# ~/.my.conf has db connection credentials
mkdir -p $BU_PATH/$YEAR/$MONTH/$DAY/$HOUR:$MIN
mysqldump -uphp-user mofet_1 | gzip > $BU_PATH/$YEAR/$MONTH/$DAY/$HOUR:$MIN/mofet_1.sql.gz
mysqldump -uphp-user mofet_1 tag | gzip > $BU_PATH/$YEAR/$MONTH/$DAY/$HOUR:$MIN/tag.sql.gz 
