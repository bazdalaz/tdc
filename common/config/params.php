<?php
return [
    'user.passwordResetTokenExpire' => 3600,
    'storagePath' => 'uploads/',
    'storageUri' => 'http://tdc.local/uploads/',
    'lcnPath' => '@data/LCNCL',
    'dbPath' => '@data/Data/LCNDB',
    'aPath' => '@data/Data/DataA',
    'bPath' => '@data/Data/DataB',
    'ncfPath' => '@data/Data/LCNSYS',
    'supportEmail' => 'support@ilnhv-mofet',
];
