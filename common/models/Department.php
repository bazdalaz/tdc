<?php

namespace common\models;

/**
 * This is the model class for table "department".
 *
 * @property int $id
 * @property string $department_name
 * @property string $department_alias
 * @property string $description
 */
class Department extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'department';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['department_name', 'department_alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'department_name' => 'Department Name',
            'department_alias' => 'Department Alias',
            'description' => 'Description',
        ];
    }
}
