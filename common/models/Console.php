<?php

namespace common\models;

/**
 * This is the model class for table "console".
 *
 * @property int $id
 * @property int $net_id
 * @property int $console_num
 * @property string $description
 * @property string $notes
 */
class Console extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'console';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['net_id', 'console_num'], 'integer'],
            [['description', 'notes'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'net_id' => 'Net ID',
            'console_num' => 'Console Num',
            'description' => 'Description',
            'notes' => 'Notes',
        ];
    }

    public function getNet()
    {
        return $this->hasOne(Net::className(), ['net_id' => 'id']);
    }
}
