<?php

namespace common\models;

/**
 * This is the model class for table "area".
 *
 * @property int $id
 * @property int $net_id
 * @property int $area_num
 * @property string $area_name
 * @property string $area_description
 */
class Area extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'area';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['net_id', 'area_num'], 'integer'],
            [['area_name', 'area_description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'net_id' => 'Net ID',
            'area_num' => 'Area Num',
            'area_name' => 'Area Name',
            'area_description' => 'Area Description',
        ];
    }

    public function getNet()
    {
        return $this->hasOne(Net::className(), ['net_id' => 'id']);
    }
}
