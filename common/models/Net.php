<?php

namespace common\models;

use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "net".
 *
 * @property int $id
 * @property string $type
 * @property string $name
 * @property string $time
 * @property string $description
 * @property string $cable
 * @property string $status
 * @property string $errors
 */
class Net extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'net';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['time'], 'safe'],
            [['description', 'errors'], 'string'],
            [['type', 'name', 'cable', 'status'], 'string', 'max' => 8],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'name' => 'Name',
            'time' => 'Time',
            'description' => 'Description',
            'cable' => 'Cable',
            'status' => 'Status',
            'errors' => 'Errors',
        ];
    }

    public static function findNet($id)
    {
        if ($net = Net::findOne($id)) {
            return $net;
        }
        throw NotFoundHttpException();
    }

    public function getNodes()
    {
        return $this->hasMany(Node::className(), ['net_id' => 'id']);
    }

    public function getPms()
    {
        return $this->hasMany(Pm::className(), ['net_id' => 'id']);
    }

    public function getNetName()
    {
        if (isset($this->id)) {
            return Net::findOne($this->id)->name;
        }
        return 'n/a';
    }

    public function getUcns()
    {
        return $this->hasMany(Ucn::className(), ['net_id' => 'id']);
    }

    public static function getNims($net_id)
    {
        return Node::find()->where(['net_id' => $net_id])->andWhere('ucn_id IS NOT NULL')->all();
    }

    public static function getNetList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
}
