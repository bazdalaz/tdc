<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TagSearch represents the model behind the search form of `common\models\Tag`.
 */
class TagSearch extends Tag
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type_id', 'net_id', 'ucn_id', 'pm_num', 'slot_id', 'slot_di_1', 'slot_di_2', 'slot_do_1', 'slot_do_2', 'slot_ao_1', 'status_id', 'updated_at', 'created_at'], 'integer'],
            [['tag_name', 'description', 'plant', 'card_id', 'card_di_1', 'card_di_2', 'card_do_1', 'card_do_2', 'ci_1', 'card_ao_1'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tag::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type_id' => $this->type_id,
            'net_id' => $this->net_id,
            'ucn_id' => $this->ucn_id,
            'pm_num' => $this->pm_num,
            'slot_id' => $this->slot_id,
            'slot_di_1' => $this->slot_di_1,
            'slot_di_2' => $this->slot_di_2,
            'slot_do_1' => $this->slot_do_1,
            'slot_do_2' => $this->slot_do_2,
            'slot_ao_1' => $this->slot_ao_1,
            'status_id' => $this->status_id,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'tag_name', $this->tag_name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'plant', $this->plant])
            ->andFilterWhere(['like', 'card_id', $this->card_id])
            ->andFilterWhere(['like', 'card_di_1', $this->card_di_1])
            ->andFilterWhere(['like', 'card_di_2', $this->card_di_2])
            ->andFilterWhere(['like', 'card_do_1', $this->card_do_1])
            ->andFilterWhere(['like', 'card_do_2', $this->card_do_2])
            ->andFilterWhere(['like', 'ci_1', $this->ci_1])
            ->andFilterWhere(['like', 'card_ao_1', $this->card_ao_1]);

        return $dataProvider;
    }
}
