<?php

namespace common\models;

/**
 * This is the model class for table "jb_point".
 *
 * @property int $id
 * @property string $tag_name
 * @property int $tag_card
 * @property int $tag_point
 * @property int $jb_id
 * @property int $jb_point
 * @property int $ok
 * @property string $free
 * @property string $updated_at
 * @property string $created_at
 */
class JbPoint extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jb_point';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tag_card', 'tag_point', 'jb_id', 'jb_point', 'ok'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['tag_name', 'free'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tag_name' => 'Tag Name',
            'tag_card' => 'Tag Card',
            'tag_point' => 'Tag Point',
            'jb_id' => 'Jb ID',
            'jb_point' => 'Jb Point',
            'ok' => 'Ok',
            'free' => 'Free',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }
}
