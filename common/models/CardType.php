<?php

namespace common\models;

/**
 * This is the model class for table "card_type".
 *
 * @property int $type_id
 * @property string $type_name
 * @property string $type_alias
 * @property int $slot_count
 * @property string $description
 */
class CardType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'card_type';
    }

    // public function getCards()
    // {
    //     return $this->hasMany(Card::className(), ['card_type' => 'type_id']);
    // }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['slot_count', 'type_id'], 'integer'],
            [['description'], 'string'],
            [['type_name'], 'string', 'max' => 16],
            [['type_alias'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'type_id' => 'Type ID',
            'type_name' => 'Type Name',
            'type_alias' => 'Type Alias',
            'slot_count' => 'Slot Count',
            'description' => 'Description',
        ];
    }
}
