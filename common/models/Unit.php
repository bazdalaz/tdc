<?php

namespace common\models;

/**
 * This is the model class for table "unit".
 *
 * @property int $id
 * @property int $net_id
 * @property int $unit_num
 * @property string $unit_description
 */
class Unit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'unit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['net_id', 'unit_num'], 'integer'],
            [['unit_description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'net_id' => 'Net ID',
            'unit_num' => 'Unit Num',
            'unit_description' => 'Unit Description',
        ];
    }

    public function getNet()
    {
        return $this->hasOne(Net::className(), ['net_id' => 'id']);
    }
}
