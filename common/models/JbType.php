<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "jb_type".
 *
 * @property int $type_id
 * @property string $type_name
 * @property int $point_cnt
 * @property string $description
 */
class JbType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jb_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['point_cnt'], 'integer'],
            [['description'], 'string'],
            [['type_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'type_id' => 'Type ID',
            'type_name' => 'Type Name',
            'point_cnt' => 'Point Cnt',
            'description' => 'Description',
        ];
    }

    public static function getTypeList()
    {
        return ArrayHelper::map(self::find()->all(), 'type_id', 'type_name');
    }
}
