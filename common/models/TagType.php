<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tag_type".
 *
 * @property int $type_id
 * @property string $type_name
 * @property string $type_alias
 * @property string $description
 */
class TagType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tag_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['type_name'], 'string', 'max' => 16],
            [['type_alias'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'type_id' => 'Type ID',
            'type_name' => 'Type Name',
            'type_alias' => 'Type Alias',
            'description' => 'Description',
        ];
    }

    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['type_id' => 'type_id']);
    }

    public static function getTypeList()
    {
        return ArrayHelper::map(self::find()->all(), 'type_id', 'type_name');
    }

    public static function getTypeId($type_alias)
    {
        return self::findOne(['type_alias' => $type_alias])->type_id;
    }
}
