<?php

namespace common\models;

use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "pm".
 *
 * @property int $id
 * @property int $pm_num
 * @property int $net_id
 * @property int $ucn_id
 * @property string $type
 * @property int $rpv_cnt
 * @property int $rc_cnt
 * @property int $dc_cnt
 * @property int $seq_cnt
 * @property int $num_cnt
 * @property int $str_cnt
 * @property int $time_cnt
 * @property int $arr_cnt
 * @property int $scan_per
 */
class Pm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pm_num', 'net_id', 'ucn_id', 'rpv_cnt', 'rc_cnt', 'dc_cnt', 'seq_cnt', 'num_cnt', 'str_cnt', 'time_cnt', 'arr_cnt', 'scan_per'], 'integer'],
            [['type'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pm_num' => 'Pm Num',
            'net_id' => 'Net ID',
            'ucn_id' => 'Ucn ID',
            'type' => 'Type',
            'rpv_cnt' => 'Rpv Cnt',
            'rc_cnt' => 'Rc Cnt',
            'dc_cnt' => 'Dc Cnt',
            'seq_cnt' => 'Seq Cnt',
            'num_cnt' => 'Num Cnt',
            'str_cnt' => 'Str Cnt',
            'time_cnt' => 'Time Cnt',
            'arr_cnt' => 'Arr Cnt',
            'scan_per' => 'Scan Per',
        ];
    }

    public static function findPms($net_id, $ucn_id)
    {
        if ($pms = PM::findAll(['net_id' => $net_id, 'ucn_id' => $ucn_id])) {
            return $pms;
        }
        throw new NotFoundHttpException();
    }

    public static function getPmById($pm_id)
    {
        $net_id = explode('-', $pm_id)[0];
        $ucn_id = explode('-', $pm_id)[1];
        $pm_num = explode('-', $pm_id)[2];

        if ($pm = PM::findOne(['net_id' => $net_id, 'ucn_id' => $ucn_id, 'pm_num' => $pm_num])) {
            return $pm;
        }
        throw new NotFoundHttpException();
    }

    public function getPlant()
    {
        if ($pm = Plant::findOne(['net_id' => $this->net_id, 'ucn_id' => $this->ucn_id])) {
            return $pm;
        }
        throw NotFoundHttpException();
    }

    public function getNetName()
    {
        if ($net = Net::findOne(['id' => $this->net_id])) {
            return $net->name;
        }
        throw NotFoundHttpException();
    }

    public static function getPmList($plant)
    {
        if ($plant) {
            return ArrayHelper::map(self::find()->where(['plant' => $plant])->all(), 'pm_num', 'pm_num');
        }

        return ArrayHelper::map(self::find()->all(), 'pm_num', 'pm_num');
    }

    public function usedDcCount()
    {
        if ($tagCount = Tag::find()->where(['type_id' => 9])
            ->andWhere(['status_id' => [1, 3, 4, 5]])
            ->andWhere(['net_id' => $this->net_id])
            ->andWhere(['ucn_id' => $this->ucn_id])
            ->andWhere(['pm_num' => $this->pm_num])->count()) {
            return $tagCount;
        }
    }

    public function usedRpvCount()
    {
        if ($tagCount = Tag::find()->where(['type_id' => 8])
            ->andWhere(['status_id' => [1, 3, 4, 5]])
            ->andWhere(['net_id' => $this->net_id])
            ->andWhere(['ucn_id' => $this->ucn_id])
            ->andWhere(['pm_num' => $this->pm_num])->count()) {
            return $tagCount;
        }
    }

    public function usedRcCount()
    {
        if ($tagCount = Tag::find()->where(['type_id' => 7])
            ->andWhere(['status_id' => [1, 3, 4, 5]])
            ->andWhere(['net_id' => $this->net_id])
            ->andWhere(['ucn_id' => $this->ucn_id])
            ->andWhere(['pm_num' => $this->pm_num])->count()) {
            return $tagCount;
        }
    }

    public function usedPrModCount()
    {
        if ($tagCount = Tag::find()->where(['type_id' => 10])
            ->andWhere(['status_id' => [1, 3, 4, 5]])
            ->andWhere(['net_id' => $this->net_id])
            ->andWhere(['ucn_id' => $this->ucn_id])
            ->andWhere(['pm_num' => $this->pm_num])->count()) {
            return $tagCount;
        }
    }

    // AI Count
    public function usedAiCount()
    {
        if ($tagCount = Tag::find()->where(['type_id' => [1]])
            ->andWhere(['status_id' => [1, 3, 4, 5]])
            ->andWhere(['net_id' => $this->net_id])
            ->andWhere(['ucn_id' => $this->ucn_id])
            ->andWhere(['pm_num' => $this->pm_num])->count()) {
            return $tagCount;
        }
    }

    public function aiCount()
    {
        $pointCount = 0;
        if ($cards = Card::find()->where(['card_type' => [1, 2]])
            ->andWhere(['net_id' => $this->net_id])
            ->andWhere(['ucn_id' => $this->ucn_id])
            ->andWhere(['pm_num' => $this->pm_num])->all()) {
            foreach ($cards as $card) {
                $pointCount += $card->slot_count;
            }
            return $pointCount;
        }
    }

    //AO Count
    public function usedAoCount()
    {
        if ($tagCount = Tag::find()->where(['type_id' => [4, 7]])
            ->andWhere(['status_id' => [1, 3, 4, 5]])
            ->andWhere(['net_id' => $this->net_id])
            ->andWhere(['ucn_id' => $this->ucn_id])
            ->andWhere(['pm_num' => $this->pm_num])
            ->andWhere(['REGEXP', 'card_ao_1', '^[0-9]*$'])
            ->count()) {
            return $tagCount;
        }
    }

    public function aoCount()
    {
        $pointCount = 0;
        if ($cards = Card::find()->where(['card_type' => [6, 7]])
            ->andWhere(['net_id' => $this->net_id])
            ->andWhere(['ucn_id' => $this->ucn_id])
            ->andWhere(['pm_num' => $this->pm_num])->all()) {
            foreach ($cards as $card) {
                $pointCount += $card->slot_count;
            }
            return $pointCount;
        }
    }

    //DI Count
    public function usedDiCount()
    {
        $tagCount1 = Tag::find()->where(['type_id' => [9]])
            ->andWhere(['status_id' => [1, 3, 4, 5]])
            ->andWhere(['net_id' => $this->net_id])
            ->andWhere(['ucn_id' => $this->ucn_id])
            ->andWhere(['pm_num' => $this->pm_num])
            ->andWhere(['REGEXP', 'card_di_1', '^[0-9]*$'])
            ->count();

        $tagCount2 = Tag::find()->where(['type_id' => [9]])
            ->andWhere(['status_id' => [1, 3, 4, 5]])
            ->andWhere(['net_id' => $this->net_id])
            ->andWhere(['ucn_id' => $this->ucn_id])
            ->andWhere(['pm_num' => $this->pm_num])
            ->andWhere(['REGEXP', 'card_di_2', '^[0-9]*$'])
            ->count();

        $tagCount3 = Tag::find()->where(['type_id' => [2]])
            ->andWhere(['status_id' => [1, 3, 4, 5]])
            ->andWhere(['net_id' => $this->net_id])
            ->andWhere(['ucn_id' => $this->ucn_id])
            ->andWhere(['pm_num' => $this->pm_num])
            ->count();

        return $tagCount1 + $tagCount2 + $tagCount3;
    }

    public function diCount()
    {
        $pointCount = 0;
        if ($cards = Card::find()->where(['card_type' => [3]])
            ->andWhere(['net_id' => $this->net_id])
            ->andWhere(['ucn_id' => $this->ucn_id])
            ->andWhere(['pm_num' => $this->pm_num])->all()) {
            foreach ($cards as $card) {
                $pointCount += $card->slot_count;
            }
            return $pointCount;
        }
    }

    //DO Count
    public function usedDoCount()
    {
        $tagCount1 = Tag::find()->where(['type_id' => [9]])
            ->andWhere(['status_id' => [1, 3, 4, 5]])
            ->andWhere(['net_id' => $this->net_id])
            ->andWhere(['ucn_id' => $this->ucn_id])
            ->andWhere(['pm_num' => $this->pm_num])
            ->andWhere(['REGEXP', 'card_do_1', '^[0-9]*$'])
            ->count();

        $tagCount2 = Tag::find()->where(['type_id' => [9]])
            ->andWhere(['status_id' => [1, 3, 4, 5]])
            ->andWhere(['net_id' => $this->net_id])
            ->andWhere(['ucn_id' => $this->ucn_id])
            ->andWhere(['pm_num' => $this->pm_num])
            ->andWhere(['REGEXP', 'card_do_2', '^[0-9]*$'])
            ->count();

        $tagCount3 = Tag::find()->where(['type_id' => [3]])
            ->andWhere(['status_id' => [1, 3, 4, 5]])
            ->andWhere(['net_id' => $this->net_id])
            ->andWhere(['ucn_id' => $this->ucn_id])
            ->andWhere(['pm_num' => $this->pm_num])
            ->count();

        return $tagCount1 + $tagCount2 + $tagCount3;
    }

    public function doCount()
    {
        $pointCount = 0;
        if ($cards = Card::find()->where(['card_type' => [4, 5]])
            ->andWhere(['net_id' => $this->net_id])
            ->andWhere(['ucn_id' => $this->ucn_id])
            ->andWhere(['pm_num' => $this->pm_num])->all()) {
            foreach ($cards as $card) {
                $pointCount += $card->slot_count;
            }

            return $pointCount;
        }
    }

    public function getCards()
    {
        if ($cards = Card::find()->where(['net_id' => $this->net_id])
            ->andWhere(['ucn_id' => $this->ucn_id])
            ->andWhere(['pm_num' => $this->pm_num])->all()) {
            return $cards;
        }
    }
}
