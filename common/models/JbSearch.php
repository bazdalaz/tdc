<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * JbSearch represents the model behind the search form of `common\models\Jb`.
 */
class JbSearch extends Jb
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'jb_cab', 'plant', 'type_id'], 'integer'],
            [['jb_num', 'elevation', 'v', 'h', 'location', 'notes'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Jb::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'jb_cab' => $this->jb_cab,
            'plant' => $this->plant,
            'type_id' => $this->type_id,
        ]);

        $query->andFilterWhere(['like', 'jb_num', $this->jb_num])
            ->andFilterWhere(['like', 'elevation', $this->elevation])
            ->andFilterWhere(['like', 'v', $this->v])
            ->andFilterWhere(['like', 'h', $this->h])
            ->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'notes', $this->notes]);

        return $dataProvider;
    }
}
