<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "card".
 *
 * @property int $net_id
 * @property int $ucn_id
 * @property int $pm_num
 * @property int $card_num
 * @property string $card_type
 * @property int $slot_count
 */
class Card extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'card';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['net_id', 'ucn_id', 'pm_num', 'card_num'], 'required'],
            [['net_id', 'ucn_id', 'pm_num', 'card_num', 'slot_count'], 'integer'],
            ['card_type', 'string', 'max' => 11],
            [['net_id', 'ucn_id', 'pm_num', 'card_num'], 'unique', 'targetAttribute' => ['net_id', 'ucn_id', 'pm_num', 'card_num']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'net_id' => 'Net ID',
            'ucn_id' => 'Ucn ID',
            'pm_num' => 'Pm Num',
            'card_num' => 'Card Num',
            'card_type' => 'Card Type',
            'slot_count' => 'Slot Count',
        ];
    }

    public function getPm()
    {
        return $this->hasOne(Pm::className(), ['net_id' => 'net_id', 'ucn_id' => 'ucn_id', 'pm_num' => 'pm_num']);
    }

    public function getType()
    {
        return $this->hasOne(CardType::className(), ['type_id' => 'card_type']);
    }

    public function getSlotCount()
    {
        return $this->slot_count;
    }

    public function getPlant()
    {
        return $this->hasOne(Plant::className(), ['net_id' => 'net_id', 'ucn_id' => 'ucn_id']);
    }

    public static function findCard($pm_id, $card_num)
    {
        $addr_array = explode('-', $pm_id);
        $net_id = $addr_array[0];
        $ucn_id = $addr_array[1];
        $pm_num = $addr_array[2];

        return Card::findOne(['net_id' => $net_id, 'ucn_id' => $ucn_id, 'pm_num' => $pm_num, 'card_num' => $card_num]);
    }

    public function getTags()
    {
        switch ($this->card_type) {
            case 3:
            case 3:
            return Tag::find()
            ->where([
                'net_id' => $this->net_id,
                'ucn_id' => $this->ucn_id,
                'pm_num' => $this->pm_num,
                'card_di_1' => $this->card_num])
            ->orWhere([
                'net_id' => $this->net_id,
                'ucn_id' => $this->ucn_id,
                'pm_num' => $this->pm_num,
                'card_di_2' => $this->card_num])->all();
        case 4:
        case 5:
            return Tag::find()
            ->where([
                'net_id' => $this->net_id,
                'ucn_id' => $this->ucn_id,
                'pm_num' => $this->pm_num,
                'card_do_1' => $this->card_num])
            ->orWhere([
                'net_id' => $this->net_id,
                'ucn_id' => $this->ucn_id,
                'pm_num' => $this->pm_num,
                'card_do_2' => $this->card_num])->all();
            case 6:
            case 7:
                return Tag::findAll([
                    'net_id' => $this->net_id,
                    'ucn_id' => $this->ucn_id,
                    'pm_num' => $this->pm_num,
                    'card_ao_1' => $this->card_num,
                    'type_id' => [4, 7]]);
            case 1:
            case 2:
                return Tag::findAll([
                    'net_id' => $this->net_id,
                    'ucn_id' => $this->ucn_id,
                    'pm_num' => $this->pm_num,
                    'card_id' => $this->card_num,
                    'type_id' => [1]]);

            default:
                return $this->card_type;
                break;
        }
    }

    public function getSlots()
    {
        $tags = $this->tags;
        $slots = [];

        for ($i = 1; $i <= $this->slot_count; $i++) {
            ArrayHelper::setValue($slots, $i, [$i => null]);
        }

        if (is_array($tags)) {
            foreach ($tags as $tag) {
                if (isset($tag->slot_di_1) && $this->card_type == 3) {
                    ArrayHelper::setValue($slots, $tag->slot_di_1, [$tag->slot_di_1 => $tag->tag_name]);
                }
                if (isset($tag->slot_di_2) && $this->card_type == 3) {
                    ArrayHelper::setValue($slots, $tag->slot_di_2, [$tag->slot_di_2 => $tag->tag_name]);
                }
                if ((isset($tag->slot_do_1)) && ($this->card_type == 4 || $this->card_type == 5)) {
                    ArrayHelper::setValue($slots, $tag->slot_do_1, [$tag->slot_do_1 => $tag->tag_name]);
                }
                if ((isset($tag->slot_do_2)) && ($this->card_type == 4 || $this->card_type == 5)) {
                    ArrayHelper::setValue($slots, $tag->slot_do_2, [$tag->slot_do_2 => $tag->tag_name]);
                }
                if ((isset($tag->slot_ao_1)) && ($this->card_type == 6 || $this->card_type == 7)) {
                    ArrayHelper::setValue($slots, $tag->slot_ao_1, [$tag->slot_ao_1 => $tag->tag_name]);
                }
                if ((isset($tag->card_id)) && ($tag->type_id == 1)) {
                    ArrayHelper::setValue($slots, $tag->slot_id, [$tag->slot_id => $tag->tag_name]);
                }
            }
        }

        return $slots;
    }

    public function countUsedSlots()
    {
        $res = 0;
        foreach ($this->slots as $key => $slot) {
            if ((ArrayHelper::getValue($slot, $key))) {
                ++$res;
            }
        }
        return $res;
    }
}
