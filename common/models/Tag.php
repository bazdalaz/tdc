<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

define('EXIST', 1);
define('DELETED', 2);
define('MANUAL_CREATED', 3);
define('MANUAL_UPDATED', 4);
define('EXIST_2DELETE', 5);
define('NOT_UPDATED', 6);

/**
 * This is the model class for table "tag".
 *
 * @property int $id
 * @property string $tag_name
 * @property string $description
 * @property string $plant
 * @property int $type_id
 * @property int $net_id
 * @property int $ucn_id
 * @property int $pm_num
 * @property string $card_id
 * @property int $slot_id
 * @property string $card_di_1
 * @property int $slot_di_1
 * @property string $card_di_2
 * @property int $slot_di_2
 * @property string $card_do_1
 * @property int $slot_do_1
 * @property string $card_do_2
 * @property int $slot_do_2
 * @property string $ci_1
 * @property string $card_ao_1
 * @property int $slot_ao_1
 */
class Tag extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            // [
            //     'class' => TimestampBehavior::className(),
            //     'attributes' => [
            //         ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
            //       //  ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
            //     ]
            // ]
            //TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tag';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_id', 'net_id', 'ucn_id', 'pm_num', 'slot_id',
                'slot_di_2', 'slot_do_1', 'slot_do_2', 'slot_di_1',
                'slot_ao_1', 'status_id'], 'integer'],
            [['tag_name', 'description', 'plant', 'card_id', 'card_di_1', 'card_di_2',
                'card_do_1', 'card_do_2', 'ci_1', 'note',
                'card_ao_1', 'updated_by'], 'string', 'max' => 64],
            [['tag_name'], 'unique', ],
            [['tag_name'], 'required'],
            [['ai_hi', 'ai_low'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tag_name' => 'Tag Name',
            'description' => 'Description',
            'plant' => 'Plant',
            'type_id' => 'Type ID',
            'net_id' => 'Net ID',
            'ucn_id' => 'Ucn ID',
            'pm_num' => 'Pm Num',
            'card_id' => 'Card ID',
            'slot_id' => 'Slot ID',
            'card_di_1' => 'Card Di 1',
            'slot_di_1' => 'Slot Di 1',
            'card_di_2' => 'Card Di 2',
            'slot_di_2' => 'Slot Di 2',
            'card_do_1' => 'Card Do 1',
            'slot_do_1' => 'Slot Do 1',
            'card_do_2' => 'Card Do 2',
            'slot_do_2' => 'Slot Do 2',
            'ci_1' => 'Ci 1',
            'card_ao_1' => 'Card Ao 1',
            'slot_ao_1' => 'Slot Ao 1',
        ];
    }

    public function getType()
    {
        return $this->hasOne(TagType::className(), ['type_id' => 'type_id']);
    }

    public function getStatus()
    {
        return $this->hasOne(TagStatus::className(), ['id' => 'status_id']);
    }

    public function getUcn()
    {
        return $this->hasOne(Ucn::className(), ['net_id' => 'net_id', 'ucn_id' => 'ucn_id']);
    }

    public function getNet()
    {
        return $this->hasOne(Net::className(), ['id' => 'net_id']);
    }

    public static function getIdByName($tag_name)
    {
        return Tag::findOne(['tag_name' => $tag_name])->id;
    }

    public static function getDeletedTags()
    {
        return Tag::findAll(['status_id' => DELETED]);
    }

    public static function markTagsDeleted()
    {
        Tag::updateAll(['status_id' => DELETED], ['not in', 'status_id', [
            EXIST, DELETED, MANUAL_UPDATED, MANUAL_CREATED, EXIST_2DELETE,
        ]]);
    }

    public static function markTagBeforeUpdate()
    {
        Tag::updateAll(['status_id' => NOT_UPDATED], 'status_id =' . EXIST);
    }

    public static function getClassByStatus($point)
    {
        if ($tag = Tag::findOne(['tag_name' => $point])) {
            switch ($tag->status->id) {
            case '1':
            break;
            case '2':
                return 'style="text-decoration: line-through;  opacity: 0.4;"';
                break;
            case '3':
            return 'style=" opacity: 0.4;"'; //background-color:aliceblue;
            break;
            case '4':
            return 'style="opacity: 0.4;"'; //background-color:honeydew;
            break;

            default:
                 return 'style="opacity: 0;"';
                break;
        }
        } else {
            return 'style="opacity: 0;"';
        }
    }
}
