<?php

namespace common\models;

use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "node".
 *
 * @property int $id
 * @property int $net_idgit
 * @property int $area_id
 * @property int $type_id
 * @property int $node_num
 * @property int $console_id
 * @property int $station_id
 * @property int $ucn_id
 * @property int $plant_id
 */
class Node extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'node';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['net_id', 'area_id', 'type_id', 'node_num', 'console_id', 'station_id', 'ucn_id', 'plant_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'net_id' => 'Net ID',
            'area_id' => 'Area ID',
            'type_id' => 'Type ID',
            'node_num' => 'Node Num',
            'console_id' => 'Console ID',
            'station_id' => 'Station ID',
            'ucn_id' => 'Ucn ID',
            'plant_id' => 'Plant ID',
        ];
    }

    public function getTypeName($id)
    {
        if ($type = NodeType::findOne($id)->name) {
            return $type;
        }
        throw NotFoundHttpException();
    }

    public function getTypeDescription($id)
    {
        if ($description = NodeType::findOne($id)->description) {
            return $description;
        }
        throw NotFoundHttpException();
    }

    public static function findNode($id, $node_num)
    {
        if ($node = Node::findOne(['net_id' => $id, 'node_num' => $node_num])) {
            return $node;
        }
        throw NotFoundHttpException();
    }

    public function getPlantName()
    {
        if ($plantName = Plant::findOne(['id' => $this->plant_id])) {
            return $plantName->name;
        }

        if ($plants = Plant::findAll(['net_id' => $this->net_id, 'ucn_id' => $this->ucn_id])) {
            foreach ($plants as $plant) {
                $plantName[] = $plant->name;
            }

            return implode('/', $plantName);
        }
        return '..';
    }

    public function getAreaName($id, $area_id)
    {
        if ($area_name = Area::findOne(['net_id' => $id, 'area_num' => $area_id])->area_name) {
            return $area_name;
        }
        throw NotFoundHttpException();
    }

    public function getPms()
    {
        if ($pms = Pm::findAll(['net_id' => $this->net_id, 'ucn_id' => $this->ucn_id])) {
            return $pms;
        }
        // throw new NotFoundHttpException();
    }
}
