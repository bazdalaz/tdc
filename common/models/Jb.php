<?php

namespace common\models;

/**
 * This is the model class for table "jb".
 *
 * @property int $id
 * @property int $jb_num
 * @property int $jb_cab
 * @property int $plant
 * @property int $type_id
 * @property string $elevation
 * @property string $v
 * @property string $h
 * @property string $location
 * @property string $notes
 */
class Jb extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jb';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jb_num', 'jb_cab', 'plant', 'type_id'], 'integer'],
            [['location', 'notes'], 'string'],
            [['elevation', 'v', 'h'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jb_num' => 'Jb Num',
            'jb_cab' => 'Jb Cab',
            'plant' => 'Plant',
            'type_id' => 'Type ID',
            'elevation' => 'Elevation',
            'v' => 'V',
            'h' => 'H',
            'location' => 'Location',
            'notes' => 'Notes',
        ];
    }

    public function getType()
    {
        return $this->hasOne(JbType::className(), ['type_id' => 'type_id']);
    }
}
