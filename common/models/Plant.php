<?php

namespace common\models;

use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "plant".
 *
 * @property int $id
 * @property int $net_id
 * @property string $name
 * @property int $area_id
 * @property int $console_id
 * @property string $letter
 * @property string $description
 */
class Plant extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plant';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['net_id', 'area_id', 'console_id'], 'required'],
            [['net_id', 'area_id', 'console_id'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255],
            [['letter'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'net_id' => 'Net ID',
            'name' => 'Name',
            'area_id' => 'Area ID',
            'console_id' => 'Console ID',
            'letter' => 'Letter',
            'description' => 'Description',
        ];
    }

    public function getNet()
    {
        return $this->hasOne(Net::className(), ['id' => 'net_id']);
    }

    public function getNetName()
    {
        if ($net = Net::findOne($this->net_id)->name) {
            return $net;
        }
        throw NotFoundHttpException();
    }

    public function getUcn()
    {
        return $this->hasOne(Ucn::className(), ['net_id' => 'net_id', 'ucn_id' => 'ucn_id']);
    }

    public function getNims()
    {
        return $this->hasMany(Node::className(), ['net_id' => 'net_id', 'ucn_id' => 'ucn_id']);
    }

    public function getPms()
    {
        switch ($this->name) {
            case '600':
                return Pm::findAll(['net_id' => $this->net_id, 'ucn_id' => $this->ucn_id, 'pm_num' => '11']);
                break;
            case '610':
                return Pm::findAll(['net_id' => $this->net_id, 'ucn_id' => $this->ucn_id, 'pm_num' => '13']);
                break;
            case '620':
                return Pm::findAll(['net_id' => $this->net_id, 'ucn_id' => $this->ucn_id, 'pm_num' => '27']);
                break;
            case '630':
                return Pm::findAll(['net_id' => $this->net_id, 'ucn_id' => $this->ucn_id, 'pm_num' => '25']);
                break;
            case '634':
                return Pm::findAll(['net_id' => $this->net_id, 'ucn_id' => $this->ucn_id, 'pm_num' => '29']);
                break;
            case '660':
                return Pm::findAll(['net_id' => $this->net_id, 'ucn_id' => $this->ucn_id, 'pm_num' => '27']);
                break;
            case '690':
                return Pm::findAll(['net_id' => $this->net_id, 'ucn_id' => $this->ucn_id, 'pm_num' => '17']);
                break;
            case 710:
                return Pm::findAll(['net_id' => $this->net_id, 'ucn_id' => $this->ucn_id, 'pm_num' => '15']);
                break;
            case 720:
                return Pm::findAll(['net_id' => $this->net_id, 'ucn_id' => $this->ucn_id, 'pm_num' => '25']);
                break;
            case 740:
                return Pm::findAll(['net_id' => $this->net_id, 'ucn_id' => $this->ucn_id, 'pm_num' => ['15', '17']]);
                break;
            case 800:
                return Pm::findAll(['net_id' => $this->net_id, 'ucn_id' => $this->ucn_id, 'pm_num' => ['5', '7']]);
                break;
            default:
                return $this->hasMany(Pm::className(), ['net_id' => 'net_id', 'ucn_id' => 'ucn_id']);
                break;
        }
        return $this->hasMany(Pm::className(), ['net_id' => 'net_id', 'ucn_id' => 'ucn_id']);
    }

    public static function getPlantList()
    {
        return ArrayHelper::map(self::find()->all(), 'name', 'name');
    }
}
