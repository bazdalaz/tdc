<?php

namespace common\models;

use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ucn".
 *
 * @property int $id
 * @property int $ucn_id
 * @property int $net_id
 * @property int $node_id
 * @property int $pm_id
 */
class Ucn extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ucn';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ucn_id', 'net_id', 'node_id', 'pm_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ucn_id' => 'Ucn ID',
            'net_id' => 'Net ID',
            'node_id' => 'Node ID',
            'pm_id' => 'Pm ID',
        ];
    }

    public function getNodes()
    {
        return $this->hasMany(Node::className(), ['ucn_id' => 'ucn_id', 'net_id' => 'net_id']);
    }

    public function getPms()
    {
        return $this->hasMany(Pm::className(), ['ucn_id' => 'ucn_id']);
    }

    public function getNet()
    {
        return $this->hasOne(Net::className(), ['id' => 'net_id']);
    }

    public function getPlant()
    {
        return $this->hasMany(Plant::className(), ['ucn_id' => 'ucn_id']);
    }

    public static function findUcn($net_id, $ucn_id)
    {
        if ($ucn = Node::findAll(['net_id' => $net_id, 'ucn_id' => $ucn_id])) {
            return $ucn;
        }
        throw NotFoundHttpException();
    }

    public static function findNodes($net_id, $ucn_id)
    {
        if ($nodes = Node::findAll(['net_id' => $net_id, 'ucn_id' => $ucn_id])) {
            return $nodes;
        }
        throw NotFoundHttpException();
    }

    public static function getUcnList($ucn_id)
    {
        if ($ucn_id){

            return ArrayHelper::map(self::find()->where(['ucn_id' =>  $ucn_id])->all(), 'ucn_id', 'ucn_id');
        }
        return ArrayHelper::map(self::find()->all(), 'ucn_id', 'ucn_id');

    }
    
}
