<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\models\Net;
use common\models\Plant;
use common\models\Unit;
use common\models\Area;
use common\models\Console;
use common\models\Node;
use common\models\NodeType;
use common\models\Ucn;
use common\models\Pm;
use common\models\Card;
use common\models\CardType;
use common\models\Tag;
use common\models\TagType;
use console\components\StringHelper;

class UpdateController extends Controller
{
    const DATETIME_FORMAT = 'php:Y-m-d H:i:s';

    public static function getNcfPath($name)
    {
        if ($name == '2') {
            return Yii::getAlias('@ncfPathB');
        } elseif ($name == '1') {
            return Yii::getAlias('@ncfPathA');
        }
    }

    public static function getNetName($name)
    {
        if ($name == 1) {
            return 'A';
        }
        return 'B';
    }

    public function getFiles($path, $name)
    {
        $files = [];

        $dirs = ($name == 1) ? Yii::$app->params['dirs_A'] : Yii::$app->params['dirs_B'];

        foreach ($dirs as $dir) {
            $directory = $path . '/' . $dir;
            foreach (glob($directory . '/$NM*B*.EB') as $file) {
                $files[] = $file;
            }
        }

        return $files;
    }

    public function actionNcf($name)
    {
        print_r(self::parseNcf($name));
    }

    public function actionPm($name)
    {
        print_r(self::parsePm($name));
    }

    public static function parseNcf($name)
    {
        $handle = @fopen(self::getNcfPath($name), 'r');
        if ($handle) {
            Net::deleteAll('id=' . $name);
            Unit::deleteAll('net_id=' . $name);
            Area::deleteAll('net_id=' . $name);
            Console::deleteAll('net_id=' . $name);
            Node::deleteAll('net_id=' . $name);
            Ucn::deleteAll('net_id=' . $name);
            echo 'deleted';

            while (($buffer = fgets($handle, 4096)) !== false) {
                if (strpos($buffer, 'SYSTEM ID') !== false) {
                    $type = trim(substr($buffer, strpos($buffer, ':') + 2));
                }

                if (strpos($buffer, 'SYSTEM DESCRIPTOR') !== false) {
                    $description = trim(substr($buffer, strpos($buffer, ':') + 1));
                }

                if (strpos($buffer, 'PRINT TO FILE TIME STAMP :') !== false) {
                    $timeNcf = trim(substr($buffer, strpos($buffer, 'P :') + 3, 19));
                    $date = Yii::$app->formatter->asDatetime($timeNcf, 'y-M-d H:mm:ss');

                    $net = new Net();
                    $net->id = $name;
                    $net->name = self::getNetName($name);
                    $net->type = $type;
                    $net->description = $description;
                    $net->time = $date;
                    $net->insert();
                    //Net::updateAll(['time' => $date], ['id' => $name,'description' => $description]);
                }

                if (strpos($buffer, 'UNIT ID') !== false) {
                    $unit_id = trim(substr($buffer, strpos($buffer, ':') + 2));
                }
                if (strpos($buffer, 'UNIT DESCRIPTION') !== false) {
                    $unit_description = trim(substr($buffer, strpos($buffer, ':') + 2));
                    $unit = new Unit();
                    $unit->net_id = $name;
                    $unit->unit_num = $unit_id;
                    $unit->unit_description = $unit_description;
                    $unit->insert();
                }

                if (strpos($buffer, 'AREA NUMBER') !== false) {
                    $area_num = trim((substr($buffer, strpos($buffer, ':') + 2)));
                }

                if (strpos($buffer, 'AREA NAME') !== false) {
                    $area_name = trim(substr($buffer, strpos($buffer, ':') + 2));
                }
                if (strpos($buffer, 'AREA DESCRIPTION') !== false) {
                    $area_description = trim(substr($buffer, strpos($buffer, ':') + 2));

                    $area = new Area();
                    $area->net_id = $name;
                    $area->area_num = $area_num;
                    $area->area_name = $area_name;
                    $area->area_description = $area_description;
                    $area->insert();
                }

                if (strpos($buffer, 'CONSOLE NUMBER') !== false) {
                    $console_num = trim(substr($buffer, strpos($buffer, ':') + 2));
                }
                if (strpos($buffer, 'CONSOLE DESCRIPTION') !== false) {
                    $console_description = trim(substr($buffer, strpos($buffer, ':') + 2));
                    $console = new Console();
                    $console->net_id = $name;
                    $console->console_num = $console_num;
                    $console->description = $console_description;
                    $console->insert();
                }

                if (strpos($buffer, 'NODE NUMBER :') !== false) {
                    $node_num = trim(substr($buffer, strpos($buffer, ':') + 2));
                }
                if (strpos($buffer, 'NODE TYPE :') !== false) {
                    $node_type = trim(substr($buffer, strpos($buffer, ':') + 2));
                    $node = new Node();
                    $node->net_id = $name;
                    $node->node_num = $node_num;
                    $node->type_id = NodeType::findOne(['name' => $node_type])->id;
                    $node->insert();
                }

                if (strpos($buffer, 'NODE : ') !== false) {
                    $node_num = trim(substr($buffer, strpos($buffer, ':') + 2));
                    $node = Node::findOne(['node_num' => $node_num, 'net_id' => $name]);
                    if ($node) {
                        echo $node->node_num . " is now processing \n";
                    }
                }

                if (strpos($buffer, 'CONSOLE #') !== false) {
                    $cons_num = trim(substr($buffer, strpos($buffer, ':') + 2));
                    $node->console_id = $cons_num;
                }

                if (strpos($buffer, 'STATION #') !== false) {
                    $station_id = trim(substr($buffer, strpos($buffer, ':') + 2));
                    $node->station_id = $station_id;
                }

                if (strpos($buffer, 'UNIVERSAL CONTROL NETWORK') !== false) {
                    $ucn_id = trim(substr($buffer, strpos($buffer, ':') + 2));
                    $node->ucn_id = $ucn_id;
                    $node->save(false);
                    if (!(Ucn::findOne(['net_id' => $name, 'ucn_id' => $ucn_id]))) {
                        $ucn = new Ucn();
                        $ucn->net_id = $name;
                        $ucn->node_id = $node_num;
                        $ucn->ucn_id = $ucn_id;
                        $ucn->insert();
                    }
                }

                if (strpos($buffer, 'DEFAULT AREA ') !== false) {
                    $area_id = trim(substr($buffer, strpos($buffer, ':') + 2));
                    if ($area_id) {
                        $area = Area::findOne(['area_name' => $area_id, 'net_id' => $name])->area_num;
                        $plant = Plant::findOne(['area_id' => $area, 'net_id' => $name])->id;
                    }
                    $node->area_id = $area;
                    $node->plant_id = $plant;
                    $node->save(false);
                    echo "end area update \n";
                    echo '$node->node_num = ' . $node->node_num . "\n";
                    echo '$node->station_id = ' . $node->station_id . "\n";
                    echo '$node->console_id = ' . $node->console_id . "\n";
                    echo '$node->area_id = ' . $node->area_id . "\n";
                    echo '$node->type_id = ' . $node->type_id . "\n";
                }
            }

            if (!feof($handle)) {
                echo "Error: unexpected fgets() fail \n";
                echo 'path = ' . self::getncfPath($name);
                $result = false;
            }

            fclose($handle);

            return 'OK';
        }

        return 'ERROR not handeled NCF file';
    }

    public function parsePm($name)
    {
        Pm::deleteAll('net_id=' . $name);
        Card::deleteAll('net_id=' . $name);
        $files = self::getFiles(Yii::getAlias('@dbPath'), $name);

        foreach ($files as $file) {
            $plant = explode('/', $file);
            $plant = array_slice(explode('/', $file), -2, 1);
            $plant = substr($plant[0], 0, 2) . '0';

            if ($plant == '510') {
                $plant = '651';
            }
            echo '  file: ' . $file . ' plant: ' . $plant . "\n";
            self::getPmData($file, $name, $plant);
        }
    }

    public function getPmData($file, $name, $plant)
    {
        $handle = @fopen($file, 'r');
        if ($handle) {
            echo $file . " \n";
            echo 'plant = ' . $plant . " \n";
            while (($buffer = fgets($handle, 4096)) !== false) {
                if (strpos($buffer, 'NTWKNUM  =') !== false) {
                    $ucn_id = trim(substr($buffer, strpos($buffer, '=') + 2));
                }

                if (strpos($buffer, 'NODENUM  =') !== false) {
                    $pm_num = trim(substr($buffer, strpos($buffer, '=') + 2));
                }

                if (strpos($buffer, 'NODETYP  =') !== false) {
                    $type = trim(substr($buffer, strpos($buffer, '=') + 2));
                }

                if (strpos($buffer, 'NCTLSLOT =') !== false) {
                    $rc_cnt = trim(substr($buffer, strpos($buffer, '=') + 2));
                }

                if (strpos($buffer, 'NDCSLOT  = ') !== false) {
                    $dc_cnt = trim(substr($buffer, strpos($buffer, '=') + 2));
                }

                if (strpos($buffer, 'NPVSLOT  =') !== false) {
                    $rpv_cnt = trim(substr($buffer, strpos($buffer, '=') + 2));
                }

                if (strpos($buffer, 'NPMSLOT  =') !== false) {
                    $seq_cnt = trim(substr($buffer, strpos($buffer, '=') + 2));
                }

                if (strpos($buffer, 'NNUMERIC =') !== false) {
                    $num_cnt = trim(substr($buffer, strpos($buffer, '=') + 2));
                }

                if (strpos($buffer, 'NSTRING  =') !== false) {
                    $str_cnt = trim(substr($buffer, strpos($buffer, '=') + 2));
                }

                if (strpos($buffer, 'NTIME    =') !== false) {
                    $time_cnt = trim(substr($buffer, strpos($buffer, '=') + 2));
                }

                if (strpos($buffer, 'NARRSLOT =') !== false) {
                    $arr_cnt = trim(substr($buffer, strpos($buffer, '=') + 2));
                }

                if (strpos($buffer, 'SCANPER  =') !== false) {
                    $scan_per = trim(substr($buffer, strpos($buffer, '=') + 2));
                }

                if (strpos($buffer, 'IOMFILEA') !== false) {
                    preg_match('#\((.\d*)\)#', $buffer, $match);
                    $iomfilea_val = trim(explode('=', $buffer)[1]);
                    $iomfilea_num = $match[1];
                }

                if (strpos($buffer, 'IOMCARDA') !== false) {
                    $iomcarda_val = trim(explode('=', $buffer)[1]);
                }

                if (strpos($buffer, 'IOMTYPE') !== false) {
                    $iomtype = trim(explode('=', $buffer)[1]);

                    //try to load model with available id i.e. unique key
                    $card = Card::findOne(['net_id' => $name, 'ucn_id' => $ucn_id, 'pm_num' => $pm_num, 'card_num' => $iomfilea_num]);
                    //now check if the model is null
                    if (!$card) {
                        echo ' in  card save ' . "\n\n";
                        $card = new Card();
                        $card->net_id = $name;
                        $card->ucn_id = $ucn_id;
                        $card->pm_num = $pm_num;
                        $card->card_num = $iomfilea_num;
                        $card->card_file = $iomfilea_val;
                        $card->card_slot = $iomcarda_val;
                        $card->card_type = CardType::findOne(['type_name' => $iomtype])->type_id;
                        $card->slot_count = CardType::findOne(['type_name' => $iomtype])->slot_count;
                        $card->save(false);
                    } else {
                        echo 'Card not saved ' . $name . '-' . $ucn_id . '-' . $pm_num . '-' . $iomfilea_num . "\n";
                    }
                }
            }
            //try to load model with available id i.e. unique key
            $pm = Pm::findOne(['net_id' => $name, 'ucn_id' => $ucn_id, 'pm_num' => $pm_num]);
            //now check if the model is null
            if (!$pm) {
                echo ' in save ' . "\n\n";
                $pm = new Pm();
                $pm->pm_num = $pm_num;
                $pm->ucn_id = $ucn_id;
                $pm->type = $type;
                $pm->net_id = $name;
                $pm->rpv_cnt = $rpv_cnt;
                $pm->rc_cnt = $rc_cnt;
                $pm->dc_cnt = $dc_cnt;
                $pm->seq_cnt = $seq_cnt;
                $pm->num_cnt = $num_cnt;
                $pm->str_cnt = $str_cnt;
                $pm->time_cnt = $time_cnt;
                $pm->arr_cnt = $arr_cnt;
                $pm->scan_per = $scan_per;
                $pm->plant = $plant;
                $pm->save(false);
            } else {
                echo 'PM not saved ' . $name . '-' . $ucn_id . '-' . $pm_num . "\n";
            }
        }

        return 'ERROR: nod EB file handled';
    }

    public function actionDb()
    {
        print_r(self::parseDb());
    }

    public function parseDb()
    {
        Tag::markTagBeforeUpdate();
        {
            $file = Yii::getAlias('@dbTagPathA');
            self::updateTags($file);
            }

        {
            $file = Yii::getAlias('@dbTagPathB');
            self::updateTags($file);
            }
        Tag::markTagsDeleted();
        echo "\n" . 'Tags marked as deleted in TDC: ';
        print_r(Tag::getDeletedTags());
    }

    public function updateTags($file)
    {
        //$regex = "[A-Z]{1,12}_?[\d]{0,4}[A-Z]{0,12}_?[\d]{3,6}\W?\s{11}";
        $regex = "^\s{1}[A-Z0-9_-]{0,12}\s{7}";
        $handle = @fopen($file, 'r');
        $helper = new StringHelper();

        if ($handle) {
            while (($buffer = fgets($handle, 4096)) !== false) {
                if (preg_match("/$regex/", $buffer, $matches, PREG_UNMATCHED_AS_NULL)) {
                    $name = trim($matches[0]);
                    $type = TagType::getTypeId(trim(substr($buffer, 203, 11)));

                    $description = trim(substr($buffer, 28, 26));
                    $pm = trim(substr($buffer, 55, 5));
                    $card = trim(substr($buffer, 65, 5));

                    if (strpos($card, '!!!') !== false) {
                        $card = null;
                    }

                    $slot = trim(substr($buffer, 75, 5));

                    //di_1
                    preg_match_all('!\d+!', trim(substr($buffer, 86, 15)), $di_1, PREG_UNMATCHED_AS_NULL);
                    if (strpos(trim(substr($buffer, 86, 15)), 'FL(') === false) {
                        $card_di_1 = $di_1[0][0] ?? null;
                        $slot_di_1 = $di_1[0][1] ?? null;
                    } else {
                        $card_di_1 = trim(substr($buffer, 86, strpos($buffer, '.FL(') - 86));
                        $slot_di_1 = $di_1[0][2] ?? null;
                    }
                    //DI address to cards
                    if ($type == 2) {
                        $card_di_1 = $card;
                        $slot_di_1 = $slot;
                    }

                    //di_2
                    preg_match_all('!\d+!', trim(substr($buffer, 103, 15)), $di_2, PREG_UNMATCHED_AS_NULL);
                    if (strpos(trim(substr($buffer, 103, 15)), 'FL(') === false) {
                        $card_di_2 = $di_2[0][0] ?? null;
                        $slot_di_2 = $di_2[0][1] ?? null;
                    } else {
                        $card_di_2 = $helper->before('.FL', trim(substr($buffer, 103, 15)));
                        $slot_di_2 = $di_2[0][2] ?? null;
                    }

                    //do_1
                    preg_match_all('!\d+!', trim(substr($buffer, 120, 15)), $do_1, PREG_UNMATCHED_AS_NULL);
                    if (strpos(trim(substr($buffer, 120, 15)), 'FL(') === false) {
                        $card_do_1 = $do_1[0][0] ?? null;
                        $slot_do_1 = $do_1[0][1] ?? null;
                    } else {
                        $card_do_1 = $helper->before('.FL', trim(substr($buffer, 120, 15)));
                        $slot_do_1 = $do_1[0][2] ?? null;
                    }
                    if ($type == 3) {
                        $card_do_1 = $card;
                        $slot_do_1 = $slot;
                    }

                    //do_2
                    preg_match_all('!\d+!', trim(substr($buffer, 136, 15)), $do_2, PREG_UNMATCHED_AS_NULL);
                    if (strpos(trim(substr($buffer, 136, 15)), 'FL(') === false) {
                        $card_do_2 = $do_2[0][0] ?? null;
                        $slot_do_2 = $do_2[0][1] ?? null;
                    } else {
                        $card_do_2 = $helper->before('.FL', trim(substr($buffer, 136, 15)));
                        $slot_do_2 = $do_2[0][2] ?? null;
                    }

                    //ao_1
                    preg_match_all('!\d+!', trim(substr($buffer, 169, 13)), $ao_1, PREG_UNMATCHED_AS_NULL);
                    if (strpos(trim(substr($buffer, 169, 13)), 'AO') === false) {
                        $card_ao_1 = trim(substr($buffer, 169, 13));
                        $slot_ao_1 = $ao_1[0][2] ?? null;
                    } else {
                        $card_ao_1 = $ao_1[0][0] ?? null;
                        $slot_ao_1 = $ao_1[0][1] ?? null;
                    }
                    //ao tag
                    if ($type == 4) {
                        $card_ao_1 = $card;
                        $slot_ao_1 = $slot;
                    }
                    if ((strpos($card_ao_1, '!!!') !== false) || (strpos($card_ao_1, '@@@') !== false)) {
                        $card_ao_1 = null;
                    }
                    //ci_1
                    $ci_1 = trim(substr($buffer, 154, 13));
                    if ((strpos($ci_1, '!!!') !== false) || (strpos($ci_1, '@@@') !== false)) {
                        $ci_1 = null;
                    }

                    $plant = self::getPlantByName($name);
                    if (isset($plant)) {
                    } else {
                        $plant = self::getPlantByNum($name);
                    }
                    $net = $plant->net_id;
                    $ucn = $plant->ucn_id;
                    $plant = $plant->name;

                    $status_id = EXIST;

                    $values = [
                        'tag_name' => $name,
                        'type_id' => $type,
                        'description' => $helper->convert_to($description, 'utf8'),
                        'net_id' => $net,
                        'ucn_id' => $ucn,
                        'pm_num' => $pm,
                        'plant' => $plant,
                        'card_di_1' => $card_di_1,
                        'slot_di_1' => $slot_di_1,
                        'card_di_2' => $card_di_2,
                        'slot_di_2' => $slot_di_2,
                        'card_do_1' => $card_do_1,
                        'slot_do_1' => $slot_do_1,
                        'card_do_2' => $card_do_2,
                        'slot_do_2' => $slot_do_2,
                        'card_id' => $card,
                        'slot_id' => $slot,
                        'ci_1' => $ci_1,
                        'card_ao_1' => $card_ao_1,
                        'slot_ao_1' => $slot_ao_1,
                        'status_id' => $status_id,
                        'updated_by' => 'System',
                    ];

                    //try to load model with available id i.e. unique key
                    $tag = Tag::findOne(['tag_name' => $name]);
                    //now check if the model is null
                    if (!$tag) {
                        $tag = new Tag();
                        $tag->attributes = $values;
                        // $tag->tag_name = $name;
                        if ($tag->save(false)) {
                            echo ' new tag ' . $tag->tag_name . ' saved' . "\n\n";
                        }
                    } else {
                        $tag->attributes = $values;
                        if ($tag->update(false)) {
                            echo ' existing tag updated ' . $name . "\n";
                        } else {
                            // $tag->touch('updated_at');
                            echo ' nothing to update for ' . $name . "\n";
                        }
                    }
                }
            }
        }
        if (!feof($handle)) {
            echo "Error: unexpected fgets() fail\n";

            return 'ERROR: no EB file handled';
        }
        fclose($handle);
    }

    public static function getPlantByName($tag)
    {
        switch ($tag[0]) {
                case 'A':
                    $plant_name = 600;
                    break;
                case 'H':
                    $plant_name = 610;
                    break;
                case 'B':
                    $plant_name = 700;
                    break;
                case 'D':
                    $plant_name = 640;
                    break;
                case 'O':
                    $plant_name = 630;
                    break;
                case 'W':
                    $plant_name = 634;
                    break;
                case 'Q':
                    $plant_name = 650;
                    break;
                case 'U':
                    $plant_name = 651;
                    break;
                case 'Y':
                    $plant_name = 660;
                    break;
                case 'V':
                    $plant_name = 690;
                    break;
                case 'S':
                    $plant_name = 710;
                    break;
                case 'N':
                    $plant_name = 720;
                    break;
                case 'G':
                    $plant_name = 730;
                    break;
                case 'R':
                    $plant_name = 740;
                    break;
                case 'J':
                    $plant_name = 800;
                    break;
                case 'M':
                    $plant_name = 930;
                    break;
                default:
                    echo 'DB UPDATE:Error parsing string ' . "\n";
                    break;
                    }

        if (isset($plant_name)) {
            return Plant::findOne(['name' => $plant_name]);
        }
    }

    public function getPlantByNum($tag)
    {
        switch (substr($tag, 0, 2)) {
                case '60':
                    $plant_name = 600;
                    break;
                case '61':
                    $plant_name = 610;
                    break;
                case '70':
                    $plant_name = 700;
                    break;
                case '64':
                    $plant_name = 640;
                    break;
                case '63':
                    $plant_name = 630;
                    break;
                case '44':
                    $plant_name = 634;
                    break;
                case '65':
                    $plant_name = 650;
                    break;
                case '51':
                    $plant_name = 651;
                    break;
                case '66':
                    $plant_name = 660;
                    break;
                case '69':
                    $plant_name = 690;
                    break;
                case '71':
                    $plant_name = 710;
                    break;
                case '72':
                    $plant_name = 720;
                    break;
                case '73':
                    $plant_name = 730;
                    break;
                case '74':
                    $plant_name = 740;
                    break;
                case '80':
                    $plant_name = 800;
                    break;
                case '93':
                    $plant_name = 930;
                    break;
                default:
                    $plant_name = '';
                    break;
               }
        if (isset($plant_name)) {
            $plant = Plant::findOne(['name' => intval($plant_name)]);

            return $plant;
        }
    }
}
