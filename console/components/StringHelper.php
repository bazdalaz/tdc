<?php
// after ('@', 'biohazard@online.ge');
// returns 'online.ge'
// from the first occurrence of '@'

// before ('@', 'biohazard@online.ge');
// //returns 'biohazard'
// //from the first occurrence of '@'

// between ('@', '.', 'biohazard@online.ge');
// returns 'online'
// from the first occurrence of '@'

// after_last ('[', 'sin[90]*cos[180]');
// returns '180]'
// from the last occurrence of '['

// before_last ('[', 'sin[90]*cos[180]');
// returns 'sin[90]*cos['
// from the last occurrence of '['

// between_last ('[', ']', 'sin[90]*cos[180]');
// returns '180'
// from the last occurrence of '['

namespace console\components;

class StringHelper
{
    public function after($ze, $inthat)
    {
        if (!is_bool(strpos($inthat, $ze))) {
            return substr($inthat, strpos($inthat, $ze) + strlen($ze));
        }
    }

    public function after_last($ze, $inthat)
    {
        if (!is_bool(strrevpos($inthat, $ze))) {
            return substr($inthat, strrevpos($inthat, $ze) + strlen($ze));
        }
    }

    public function before($ze, $inthat)
    {
        return substr($inthat, 0, strpos($inthat, $ze));
    }

    public function before_last($ze, $inthat)
    {
        return substr($inthat, 0, strrevpos($inthat, $ze));
    }

    public function between($ze, $that, $inthat)
    {
        return before($that, after($ze, $inthat));
    }

    public function between_last($ze, $that, $inthat)
    {
        return after_last($ze, before_last($that, $inthat));
    }

    public function convert_to($source, $target_encoding)
    {
        // detect the character encoding of the incoming file
        //$encoding = mb_detect_encoding($source, 'cp1255');
        $encoding = mb_detect_encoding($source, 'ISO-8859-8');

        // escape all of the question marks so we can remove artifacts from
        // the unicode conversion process
        // $target = str_replace('?', '[question_mark]', $source);
        $target = str_replace('?', '[question_mark]', $source);

        // convert the string to the target encoding
        $target = mb_convert_encoding($target, $target_encoding, $encoding);

        // remove any question marks that have been introduced because of illegal characters
        // $target = str_replace('?', '', $target);

        // replace the token string "[question_mark]" with the symbol "?"
        $target = str_replace('[question_mark]', '?', $target);

        return $this->reverseHebrew($target);
    }

    public function reverseHebrew($text)
    {
        $words = array_reverse(explode(' ', $text));
        foreach ($words as $index => $word) {
            if ($this->isHebrew($word)) {
                $words[$index] = hebrevc($word);
                $words[$index] = $this->mbStrRev($word);
            }
        }
        return join(' ', $words);
    }

    public function isHebrew($text)
    {
        for ($i = 0, $cnt = strlen($text); $i < $cnt; ++$i) {
            if (ord($text[$i]) > 127) {
                // if(preg_match("/[\xE0-\xFA]/", $text)){
                return true;
            }
        }
        return false;
    }

    public function mbStrRev($string, $encoding = null)
    {
        if ($encoding === null) {
            $encoding = mb_detect_encoding($string);
        }

        $length = mb_strlen($string, $encoding);
        $reversed = '';
        while ($length-- > 0) {
            $reversed .= mb_substr($string, $length, 1, $encoding);
        }

        return $reversed;
    }
}
