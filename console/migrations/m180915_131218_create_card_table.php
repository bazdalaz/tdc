<?php

use yii\db\Migration;

/**
 * Handles the creation of table `card`.
 */
class m180915_131218_create_card_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('card', [
            'net_id' => $this->integer()->notNull(),
            'ucn_id' => $this->integer(11)->notNull(),
            'pm_num' => $this->integer()->notNull(),
            'card_num' => $this->integer()->notNull(),
            'card_type' => $this->integer(),
            'card_file' => $this->integer(),
            'card_slot' => $this->integer(),
            'slot_count' => $this->integer(),
            'fta_cab' => $this->string(32),
            'fta_type' => $this->string(32),
            'tbp_cab' => $this->integer(),
        ]);
        $this->addPrimaryKey('pk-net_ucn_pm_card', 'card', ['net_id', 'ucn_id', 'pm_num', 'card_num']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('card');
    }
}
