<?php

use yii\db\Migration;

/**
 * Handles the creation of table `jb`.
 */
class m181229_091808_create_jb_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('jb', [
            'id' => $this->primaryKey(),
            'jb_num' => $this->integer(),
            'jb_cab' => $this->integer(),
            'plant' => $this->integer(),
            'type_id' => $this->integer(),
            'elevation' => $this->string(),
            'v' => $this->string(),
            'h' => $this->string(),
            'location' => $this->text(),
            'notes' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('jb');
    }
}
