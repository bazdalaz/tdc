<?php

use yii\db\Migration;
use backend\models\User;

/**
 * Class m190216_121922_create_rbac_data
 */
class m190216_121922_create_rbac_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        //define permissions
        $viewTagListPermission = $auth->createPermission('viewTagList');
        $auth->add($viewTagListPermission);

        $deleteTagPermission = $auth->createPermission('deleteTag');
        $auth->add($deleteTagPermission);

        $updateTagPermission = $auth->createPermission('updateTag');
        $auth->add($updateTagPermission);

        $viewJBListPermission = $auth->createPermission('viewJBList');
        $auth->add($viewJBListPermission);

        $deleteJBPermission = $auth->createPermission('deleteJB');
        $auth->add($deleteJBPermission);

        $updateJBPermission = $auth->createPermission('updateJB');
        $auth->add($updateJBPermission);

        $viewUserListPermission = $auth->createPermission('viewUserList');
        $auth->add($viewUserListPermission);

        $deleteUserPermission = $auth->createPermission('deleteUser');
        $auth->add($deleteUserPermission);

        $updateUserPermission = $auth->createPermission('updateUser');
        $auth->add($updateUserPermission);

        $viewReportsListPermission = $auth->createPermission('viewReportsList');
        $auth->add($viewReportsListPermission);

        $deleteReportsPermission = $auth->createPermission('deleteReports');
        $auth->add($deleteReportsPermission);

        $updateReportsPermission = $auth->createPermission('updateReports');
        $auth->add($updateReportsPermission);

        $viewDbSettingsListPermission = $auth->createPermission('viewDbSettingsList');
        $auth->add($viewDbSettingsListPermission);

        $deleteDbSettingsPermission = $auth->createPermission('deleteDbSettings');
        $auth->add($deleteDbSettingsPermission);

        $updateDbSettingsPermission = $auth->createPermission('updateDbSettings');
        $auth->add($updateDbSettingsPermission);

        $viewServerSettingsListPermission = $auth->createPermission('viewServerSettingsList');
        $auth->add($viewServerSettingsListPermission);

        $deleteServerSettingsPermission = $auth->createPermission('deleteServerSettings');
        $auth->add($deleteServerSettingsPermission);

        $updateServerSettingsPermission = $auth->createPermission('updateServerSettings');
        $auth->add($updateServerSettingsPermission);

        //define roles

        $ICRole = $auth->createRole('IC');
        $auth->add($ICRole);

        $supervisorRole = $auth->createRole('supervisor');
        $auth->add($supervisorRole);

        // $managerRole = $auth->createRole('manager');
        // $auth->add($managerRole);

        $adminRole = $auth->createRole('admin');
        $auth->add($adminRole);

        //define role-permissions relations

        $auth->addChild($ICRole, $viewTagListPermission);
        $auth->addChild($ICRole, $deleteTagPermission);
        $auth->addChild($ICRole, $viewJBListPermission);
        $auth->addChild($ICRole, $deleteJBPermission);

        $auth->addChild($supervisorRole, $ICRole);
        $auth->addChild($supervisorRole, $viewReportsListPermission);
        $auth->addChild($supervisorRole, $deleteReportsPermission);
        $auth->addChild($supervisorRole, $updateReportsPermission);

        $auth->addChild($adminRole, $supervisorRole);
        $auth->addChild($adminRole, $viewDbSettingsListPermission);
        $auth->addChild($adminRole, $deleteDbSettingsPermission);
        $auth->addChild($adminRole, $updateDbSettingsPermission);
        $auth->addChild($adminRole, $viewServerSettingsListPermission);
        $auth->addChild($adminRole, $deleteServerSettingsPermission);
        $auth->addChild($adminRole, $updateServerSettingsPermission);
        $auth->addChild($adminRole, $viewUserListPermission);
        $auth->addChild($adminRole, $deleteUserPermission);
        $auth->addChild($adminRole, $updateUserPermission);

        //create admin user
        $user = new User([
            'id' => '1',
            'email' => 'bazdalaz@gmail.com',
            'username' => 'Admin',
            'password_hash' => '$2y$13$xOz2MjPduQ7lEbShXVwcpeLdq8omYrLrc3Pp5non2H0pbl.Os4vHK',
        ]);

        $user->generateAuthKey();
        $user->save();

        //Add admin role to user
        $auth->assign($adminRole, $user->getId());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190216_121922_create_rbac_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190216_121922_create_rbac_data cannot be reverted.\n";

        return false;
    }
    */
}
