<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tag_status`.
 */
class m190112_085502_create_tag_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tag_status', [
            'id' => $this->primaryKey(),
            'status' => $this->string(),
        ]);

        $this->insert('tag_status', ['id' => '1', 'status' => 'EXIST']);
        $this->insert('tag_status', ['id' => '2', 'status' => 'DELETED']);
        $this->insert('tag_status', ['id' => '3', 'status' => 'MANUAL_CREATED']);
        $this->insert('tag_status', ['id' => '4', 'status' => 'MANUAL_UPDATED']);
        $this->insert('tag_status', ['id' => '5', 'status' => 'EXIST_2DELETE']);
        $this->insert('tag_status', ['id' => '6', 'status' => 'NOT_UPDATED']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tag_status');
    }
}
