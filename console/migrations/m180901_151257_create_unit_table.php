<?php

use yii\db\Migration;

/**
 * Handles the creation of table `unit`.
 */
class m180901_151257_create_unit_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('unit', [
            'id' => $this->primaryKey(),
            'net_id' => $this->integer(11),
            'unit_num' => $this->integer(11),
            'unit_description' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('unit');
    }
}
