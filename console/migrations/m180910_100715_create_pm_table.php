<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pm`.
 */
class m180910_100715_create_pm_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pm', [
            'net_id' => $this->integer()->notNull(),
            'ucn_id' => $this->integer(11)->notNull(),
            'pm_num' => $this->integer()->notNull(),
            'plant' => $this->integer(),
            'type' => $this->string(11),
            'rpv_cnt' => $this->integer(11),
            'rc_cnt' => $this->integer(11),
            'dc_cnt' => $this->integer(11),
            'seq_cnt' => $this->integer(11),
            'num_cnt' => $this->integer(11),
            'str_cnt' => $this->integer(11),
            'time_cnt' => $this->integer(11),
            'arr_cnt' => $this->integer(11),
            'scan_per' => $this->integer(11),
        ]);
        $this->addPrimaryKey('pk-net_ucn_pm', 'pm', ['net_id', 'ucn_id', 'pm_num']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // $this->dropForeignKey('fk-pm-ucn', 'pm');
        $this->dropTable('pm');
    }
}
