<?php

use yii\db\Migration;

/**
 * Handles the creation of table `plant`.
 */
class m180901_152612_create_plant_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('plant', [
            'id' => $this->primaryKey(),
            'net_id' => $this->integer()->notNull(),
            'ucn_id' => $this->integer()->notNull(),
            'name' => $this->string(),
            'ucn_id' => $this->integer(),
            'area_id' => $this->integer()->notNull(),
            'console_id' => $this->integer()->notNull(),
            'letter' => $this->char(),
            'description' => $this->string(),
        ]);

        $this->insert('plant', ['net_id' => '1', 'ucn_id' => '13', 'name' => '600',  'area_id' => '4', 'console_id' => '1', 'letter' => 'A', 'description' => 'Plant 600']);
        $this->insert('plant', ['net_id' => '1', 'ucn_id' => '10', 'name' => '610',  'area_id' => '7', 'console_id' => '6', 'letter' => 'H', 'description' => 'Plant 610']);
        $this->insert('plant', ['net_id' => '1', 'ucn_id' => '10', 'name' => '620',  'area_id' => '3', 'console_id' => '3', 'letter' => 'N', 'description' => 'Plant 620']);
        $this->insert('plant', ['net_id' => '1', 'ucn_id' => '2', 'name' => '630',  'area_id' => '9', 'console_id' => '9', 'letter' => 'O', 'description' => 'Plant 630']);
        $this->insert('plant', ['net_id' => '1', 'ucn_id' => '2', 'name' => '634',  'area_id' => '9', 'console_id' => '9', 'letter' => 'W', 'description' => 'Plant 634']);
        $this->insert('plant', ['net_id' => '1', 'ucn_id' => '1', 'name' => '640',  'area_id' => '5', 'console_id' => '5', 'letter' => 'D', 'description' => 'Plant 640']);
        $this->insert('plant', ['net_id' => '2', 'ucn_id' => '5', 'name' => '650',  'area_id' => '6', 'console_id' => '6', 'letter' => 'Q', 'description' => 'Plant 650']);
        $this->insert('plant', ['net_id' => '2', 'ucn_id' => '10', 'name' => '651',  'area_id' => '3', 'console_id' => '3', 'letter' => 'U', 'description' => 'Tetra']);
        $this->insert('plant', ['net_id' => '1', 'ucn_id' => '2', 'name' => '660',  'area_id' => '9', 'console_id' => '9', 'letter' => 'Y', 'description' => 'Plant 660']);
        $this->insert('plant', ['net_id' => '2', 'ucn_id' => '3', 'name' => '690',  'area_id' => '4', 'console_id' => '4', 'letter' => 'V', 'description' => 'Plant 690']);
        $this->insert('plant', ['net_id' => '1', 'ucn_id' => '6', 'name' => '700',  'area_id' => '2', 'console_id' => '2', 'letter' => 'B', 'description' => 'Plant 700']);
        $this->insert('plant', ['net_id' => '1', 'ucn_id' => '10', 'name' => '710',  'area_id' => '7', 'console_id' => '6', 'letter' => 'S', 'description' => 'Plant 710']);
        $this->insert('plant', ['net_id' => '1', 'ucn_id' => '10', 'name' => '720',  'area_id' => '3', 'console_id' => '3', 'letter' => 'N', 'description' => 'Plant 720']);
        $this->insert('plant', ['net_id' => '1', 'ucn_id' => '3', 'name' => '730',  'area_id' => '1', 'console_id' => '1', 'letter' => 'G', 'description' => 'Plant 730']);
        $this->insert('plant', ['net_id' => '1', 'ucn_id' => '13', 'name' => '740',  'area_id' => '4', 'console_id' => '4', 'letter' => 'R', 'description' => 'Plant 740']);
        $this->insert('plant', ['net_id' => '2', 'ucn_id' => '3', 'name' => '800',  'area_id' => '2', 'console_id' => '2', 'letter' => 'J', 'description' => 'Plant 800']);
        $this->insert('plant', ['net_id' => '2', 'ucn_id' => '0', 'name' => '915',  'area_id' => '5', 'console_id' => '5', 'letter' => 'K', 'description' => 'Plant 915']);
        $this->insert('plant', ['net_id' => '2', 'ucn_id' => '1', 'name' => '930',  'area_id' => '1', 'console_id' => '1', 'letter' => 'M', 'description' => 'Plant 930']);
        $this->insert('plant', ['net_id' => '1', 'ucn_id' => '0', 'name' => 'DemoA',  'area_id' => '10', 'console_id' => '10', 'letter' => ' ', 'description' => 'Demo A']);
        $this->insert('plant', ['net_id' => '2', 'ucn_id' => '0', 'name' => 'DemoB',  'area_id' => '10', 'console_id' => '10', 'letter' => ' ', 'description' => 'Demo B']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('plant');
    }
}
