<?php

use yii\db\Migration;

/**
 * Handles the creation of table `jb_point`.
 */
class m181229_103935_create_jb_point_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('jb_point', [
            'id' => $this->primaryKey(),
            'tag_name' => $this->string(),
            'tag_card' => $this->integer(),
            'tag_point' => $this->integer(),
            'jb_id' => $this->integer(),
            'jb_point' => $this->integer(),
            'ok' => $this->boolean(),
            'free' => $this->string(),
            'updated_at' => $this->timestamp(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('jb_point');
    }
}
