<?php

use yii\db\Migration;

/**
 * Class m190323_082903_alter_user_table
 */
class m190323_082903_alter_tag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{tag}}', 'note', $this->text());
        $this->addColumn('{{tag}}', 'updated_by', $this->text());
        $this->addColumn('{{tag}}', 'ai_hi', $this->float());
        $this->addColumn('{{tag}}', 'ai_low', $this->float());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{tag}}', 'note');
        $this->dropColumn('{{tag}}', 'updated_by');
        $this->dropColumn('{{tag}}', 'ai_hi');
        $this->dropColumn('{{tag}}', 'ai_low');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190323_082903_alter_user_table cannot be reverted.\n";

        return false;
    }
    */
}
