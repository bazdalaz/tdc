<?php

use yii\db\Migration;

/**
 * Class m180825_125722_alter_user_table
 */
class m180825_125722_alter_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{user}}', 'phone', $this->string(20));
        $this->addColumn('{{user}}', 'type', $this->integer(3));
        $this->addColumn('{{user}}', 'fullname', $this->string(30));
        $this->addColumn('{{user}}', 'nickname', $this->string(30));
        $this->addColumn('{{user}}', 'position', $this->string(60));
        $this->addColumn('{{user}}', 'department', $this->string(60));
        $this->addColumn('{{user}}', 'about', $this->text());
        $this->addColumn('{{user}}', 'picture', $this->string());
    }

    public function down()
    {
        $this->dropColumn('{{user}}', 'phone');
        $this->dropColumn('{{user}}', 'type');
        $this->dropColumn('{{user}}', 'fullname');
        $this->dropColumn('{{user}}', 'nickname');
        $this->dropColumn('{{user}}', 'position');
        $this->dropColumn('{{user}}', 'department');
        $this->dropColumn('{{user}}', 'about');
        $this->dropColumn('{{user}}', 'picture');
    }
}
