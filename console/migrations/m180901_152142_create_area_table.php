<?php

use yii\db\Migration;

/**
 * Handles the creation of table `area`.
 */
class m180901_152142_create_area_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('area', [
            'id' => $this->primaryKey(),
            'net_id' => $this->integer(11),
            'area_num' => $this->integer(11),
            'area_name' => $this->string(),
            'area_description' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('area');
    }
}
