<?php

use yii\db\Migration;

/**
 * Handles the creation of table `console`.
 */
class m180901_150107_create_console_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('console', [
            'id' => $this->primaryKey(),
            'net_id' => $this->integer(),
            'console_num' => $this->integer(),
            'description' => $this->text(),
            'notes' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('console');
    }
}
