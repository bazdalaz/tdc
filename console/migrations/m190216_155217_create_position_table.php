<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%position}}`.
 */
class m190216_155217_create_position_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('position', [
            'position_id' => $this->primaryKey(),
            'position_name' => $this->string(),
            'description' => $this->text(),
        ]);

        $this->insert('position', ['position_id' => 1, 'position_name' => 'Instrumentation Technician', 'description' => '']);
        $this->insert('position', ['position_id' => 2, 'position_name' => 'Instrumentation Coordinator', 'description' => '']);
        $this->insert('position', ['position_id' => 3, 'position_name' => 'Infrastructure Coordinator', 'description' => '']);
        $this->insert('position', ['position_id' => 4, 'position_name' => 'Process Engeneer', 'description' => '']);
        $this->insert('position', ['position_id' => 5, 'position_name' => 'Tech. Devision Engeneer', 'description' => '']);
        $this->insert('position', ['position_id' => 6, 'position_name' => 'Operation Devision Emloyee', 'description' => '']);
        $this->insert('position', ['position_id' => 7, 'position_name' => 'Mofet Employee', 'description' => '']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%position}}');
    }
}
