<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%department}}`.
 */
class m190216_175019_create_department_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%department}}', [
            'id' => $this->primaryKey(),
            'department_name' => $this->string(),
            'department_alias' => $this->string(),
            'description' => $this->text(),
        ]);

        $this->insert('department', ['id' => 1, 'department_name' => 'HBR', 'description' => '']);
        $this->insert('department', ['id' => 2, 'department_name' => 'FR', 'description' => '']);
        $this->insert('department', ['id' => 3, 'department_name' => 'BRU', 'description' => '']);
        $this->insert('department', ['id' => 4, 'department_name' => 'IS', 'description' => '']);
        $this->insert('department', ['id' => 5, 'department_name' => 'Mofet', 'description' => '']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%department}}');
    }
}
