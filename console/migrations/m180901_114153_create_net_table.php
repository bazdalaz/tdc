<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lcn`.
 */
class m180901_114153_create_net_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('net', [
            'id' => $this->primaryKey(),
            'type' => $this->string(8),
            'name' => $this->string(8)->notNull(),
            'time' => $this->dateTime(),
            'description' => $this->text(),
            'cable' => $this->string(8),
            'status' => $this->string(8),
            'errors' => $this->text(),
        ]);

        $this->insert('net', ['id' => '1', 'type' => 'LCN', 'name' => 'A']);
        $this->insert('net', ['id' => '2', 'type' => 'LCN', 'name' => 'B']);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('net');
    }
}
