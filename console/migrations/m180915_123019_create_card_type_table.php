<?php

use yii\db\Migration;

/**
 * Handles the creation of table `card_type`.
 */
class m180915_123019_create_card_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('card_type', [
            'type_id' => $this->primaryKey(),
            'type_name' => $this->string(16),
            'type_alias' => $this->string(32),
            'slot_count' => $this->integer(),
            'description' => $this->text(),
        ]);

        $this->insert('card_type', ['type_id' => '1', 'type_name' => 'HLAI', 'type_alias' => 'Analog Input', 'slot_count' => '16', ]);
        $this->insert('card_type', ['type_id' => '2', 'type_name' => 'LLMUX', 'type_alias' => 'LLMUX', 'slot_count' => '24', ]);
        $this->insert('card_type', ['type_id' => '3', 'type_name' => 'DI', 'type_alias' => 'Digital Input', 'slot_count' => '32', ]);
        $this->insert('card_type', ['type_id' => '4', 'type_name' => 'DO', 'type_alias' => 'Digital Output', 'slot_count' => '16', ]);
        $this->insert('card_type', ['type_id' => '5', 'type_name' => 'DO_32', 'type_alias' => 'Digital Output 32', 'slot_count' => '32', ]);
        $this->insert('card_type', ['type_id' => '6', 'type_name' => 'AO', 'type_alias' => 'Analog Output', 'slot_count' => '8', ]);
        $this->insert('card_type', ['type_id' => '7', 'type_name' => 'AO_16', 'type_alias' => 'Analog Output 16', 'slot_count' => '16', ]);
        $this->insert('card_type', ['type_id' => '8', 'type_name' => 'SI', 'type_alias' => 'Serial Interface', 'slot_count' => '0', ]);
        $this->insert('card_type', ['type_id' => '9', 'type_name' => 'NONE', 'type_alias' => 'Not Installed', 'slot_count' => '0', ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('card_type');
    }
}
