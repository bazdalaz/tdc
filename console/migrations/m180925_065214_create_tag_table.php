<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tag`.
 */
class m180925_065214_create_tag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tag', [
            'id' => $this->primaryKey(),
            'tag_name' => $this->string()->unique(),
            'description' => $this->string(),
            'plant' => $this->string(),
            'type_id' => $this->integer(),
            'net_id' => $this->integer(),
            'ucn_id' => $this->integer(),
            'pm_num' => $this->integer(),
            'card_id' => $this->string(),
            'slot_id' => $this->integer(),
            'card_di_1' => $this->string(),
            'slot_di_1' => $this->integer(),
            'card_di_2' => $this->string(),
            'slot_di_2' => $this->integer(),
            'card_do_1' => $this->string(),
            'slot_do_1' => $this->integer(),
            'card_do_2' => $this->string(),
            'slot_do_2' => $this->integer(),
            'ci_1' => $this->string(),
            'card_ao_1' => $this->string(),
            'slot_ao_1' => $this->integer(),
            'status_id' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tag');
    }
}
