<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tag_type`.
 */
class m181001_051625_create_tag_type_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('tag_type', [
            'type_id' => $this->primaryKey(),
            'type_name' => $this->string(16),
            'type_alias' => $this->string(16),
            'description' => $this->text(),
        ]);

        $this->insert('tag_type', ['type_id' => 1, 'type_name' => 'AI', 'type_alias' => 'ANALGIN']);
        $this->insert('tag_type', ['type_id' => 2, 'type_name' => 'DI', 'type_alias' => 'DIGIN']);
        $this->insert('tag_type', ['type_id' => 3, 'type_name' => 'DO', 'type_alias' => 'DIGOUT']);
        $this->insert('tag_type', ['type_id' => 4, 'type_name' => 'AO', 'type_alias' => 'ANALGOUT']);
        $this->insert('tag_type', ['type_id' => 5, 'type_name' => 'FLAG', 'type_alias' => 'FLAG']);
        $this->insert('tag_type', ['type_id' => 6, 'type_name' => 'NUM', 'type_alias' => 'NUMERIC']);
        $this->insert('tag_type', ['type_id' => 7, 'type_name' => 'RC', 'type_alias' => 'REGCTL']);
        $this->insert('tag_type', ['type_id' => 8, 'type_name' => 'RPV', 'type_alias' => 'REGPV']);
        $this->insert('tag_type', ['type_id' => 9, 'type_name' => 'DC', 'type_alias' => 'DIGCOM']);
        $this->insert('tag_type', ['type_id' => 10, 'type_name' => 'SEQ', 'type_alias' => 'PROCMODL']);
        $this->insert('tag_type', ['type_id' => 11, 'type_name' => 'ARRAY', 'type_alias' => 'ARRAY']);
        $this->insert('tag_type', ['type_id' => 12, 'type_name' => 'TIMER', 'type_alias' => 'TIMER']);
        $this->insert('tag_type', ['type_id' => 13, 'type_name' => 'LOGIC', 'type_alias' => 'LOGIC']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tag_type');
    }
}
