<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ucn`.
 */
class m180910_092233_create_ucn_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('ucn', [
            'id' => $this->primaryKey(),
            'ucn_id' => $this->integer(),
            'net_id' => $this->integer(),
            'node_id' => $this->integer(),
            'pm_id' => $this->integer(),
        ]);
        //$this->addForeignKey('fk-ucn-node', 'ucn', 'node_id', 'node', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // $this->dropForeignKey('fk-ucn-node', 'ucn');
        $this->dropTable('ucn');
    }
}
