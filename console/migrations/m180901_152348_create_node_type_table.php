<?php

use yii\db\Migration;

/**
 * Handles the creation of table `node_type`.
 */
class m180901_152348_create_node_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('node_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->string(),
        ]);

        $this->insert('node_type', ['id' => '1', 'name' => 'NIM', 'description' => 'Network Interface Module']);
        $this->insert('node_type', ['id' => '2', 'name' => 'US',  'description' => 'Universal Station']);
        $this->insert('node_type', ['id' => '3', 'name' => 'HG',  'description' => 'Highway Module']);
        $this->insert('node_type', ['id' => '4', 'name' => 'NG',  'description' => 'Network GateWay']);
        $this->insert('node_type', ['id' => '5', 'name' => 'AM',  'description' => 'Application Module']);
        $this->insert('node_type', ['id' => '6', 'name' => 'HM',  'description' => 'History Module']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('node_type');
    }
}
