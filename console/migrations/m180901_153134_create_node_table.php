<?php

use yii\db\Migration;

/**
 * Handles the creation of table `node`.
 */
class m180901_153134_create_node_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('node', [
            'id' => $this->primaryKey(),
            'net_id' => $this->integer(11),
            'area_id' => $this->integer(11),
            'type_id' => $this->integer(11),
            'node_num' => $this->integer(11),
            'console_id' => $this->integer(11),
            'station_id' => $this->integer(11),
            'ucn_id' => $this->integer(11),
            'plant_id' => $this->integer(11),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('node');
    }
}
