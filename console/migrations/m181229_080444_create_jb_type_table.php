<?php

use yii\db\Migration;

/**
 * Handles the creation of table `jb_type`.
 */
class m181229_080444_create_jb_type_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('jb_type', [
            'type_id' => $this->primaryKey(),
            'type_name' => $this->string(),
            'short_name' => $this->string(),
            'point_cnt' => $this->integer(),
            'description' => $this->text(),
        ]);

        $this->insert('jb_type', ['type_id' => 1, 'type_name' => 'JBAI', 'short_name' => 'JBAI', 'point_cnt' => 16, 'description' => '']);
        $this->insert('jb_type', ['type_id' => 2, 'type_name' => 'JBD', 'short_name' => 'JBD', 'point_cnt' => 12,  'description' => '']);
        $this->insert('jb_type', ['type_id' => 3, 'type_name' => 'JBDI', 'short_name' => 'JBDI', 'point_cnt' => 16, 'description' => '']);
        $this->insert('jb_type', ['type_id' => 4, 'type_name' => 'JBI12',  'short_name' => 'JBDI', 'point_cnt' => 12, 'description' => '']);
        $this->insert('jb_type', ['type_id' => 5, 'type_name' => 'JBI16',  'short_name' => 'JBI', 'point_cnt' => 12, 'description' => '']);
        $this->insert('jb_type', ['type_id' => 6, 'type_name' => 'JBMB',  'short_name' => 'JBMB', 'point_cnt' => 16, 'description' => '']);
        $this->insert('jb_type', ['type_id' => 7, 'type_name' => 'JBMP',  'short_name' => 'JBMP', 'point_cnt' => 16, 'description' => '']);
        $this->insert('jb_type', ['type_id' => 8, 'type_name' => 'JBSV220',  'short_name' => 'JBSV', 'point_cnt' => 14, 'description' => '']);
        $this->insert('jb_type', ['type_id' => 9, 'type_name' => 'JBSV24',  'short_name' => 'JBSV', 'point_cnt' => 16, 'description' => '']);
        $this->insert('jb_type', ['type_id' => 10, 'type_name' => 'JBSV24+REL',  'short_name' => 'JBSV', 'point_cnt' => 16, 'description' => '']);
        $this->insert('jb_type', ['type_id' => 11, 'type_name' => 'JBSV24PB',  'short_name' => 'JBSV', 'point_cnt' => 16, 'description' => '']);
        $this->insert('jb_type', ['type_id' => 12, 'type_name' => 'JBSV24PB+REL',  'short_name' => 'JBSV', 'point_cnt' => 16, 'description' => '']);
        $this->insert('jb_type', ['type_id' => 13, 'type_name' => 'CabInst',  'short_name' => 'CabInst', 'point_cnt' => 64, 'description' => '']);
        $this->insert('jb_type', ['type_id' => 14, 'type_name' => 'JBT',  'short_name' => 'JBT', 'point_cnt' => 16, 'description' => '']);
        $this->insert('jb_type', ['type_id' => 15, 'type_name' => 'JBMUX',  'short_name' => 'JBMUX', 'point_cnt' => 12, 'description' => '']);
        $this->insert('jb_type', ['type_id' => 16, 'type_name' => 'RINGIO',  'short_name' => 'RINGIO', 'point_cnt' => 16, 'description' => '']);
        $this->insert('jb_type', ['type_id' => 17, 'type_name' => 'RINGEMS',  'short_name' => 'RINGEMS', 'point_cnt' => 16, 'description' => '']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('jb_type');
    }
}
